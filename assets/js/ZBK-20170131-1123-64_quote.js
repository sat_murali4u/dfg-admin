function quote_type(e) {
    if (e == "bgcolor") {
        $("#bgf").removeClass('disable');
        $("#filef").addClass('disable');
        $("#child").css('background-image', "none");
    } else if (e == "image") {
        $("#bgf").addClass('disable');
        $("#filef").removeClass('disable');
        $("#parent").css('background', "none");
    } else {
        $("#bgf").addClass('disable');
        $("#filef").addClass('disable');
        $("#child").css('background-image', "none");
        $("#parent").css('background', "none");
    }
}
function last_quote_post(e) {
    //console.log(e);
    $.ajax({
        url: 'admin.php/quotes/get_lastpost',
        data: {cat_id: e},
        type: 'post',
        success: function (response) {
//            console.log(response);
            $('#last_posted').html(response);
            if (response != 0) {
                $('#last_posted').html('<span>Last quote posted on: </span>' + response);
            } else {
                $('#last_posted').html('<span>Still no quotes posted yet on this category.</span>');
            }

        }
    });
}
function bgcolorapply(e) {
    $("#parent").css("background-color", e).css('background-image', "none");
    $("#child").css('background-image', "none");
}
function textcolorapply(e) {
    $("#child h3").css("color", e);
}
$("#quote").keyup(function () {
    $("#quotegen").text($("#quote").val());
});
$(document).ready(function () {
    $('.last_quote').hide();
});
$("#submit").click(function () {
//    var name = $("#name").val();
    var quote_type = $('input[name=quote_type]:checked').val();
    var cat_id = $("#cat_id").val();
    var q_type = $("#q_type").val();
    var quote = $("#quote").val();
    var bgcolor = $("#bgcolor").val();
    var bg_image = $("#bg_image").val();
    var err = 0;
    if (!quote_type) {
        toastr.warning("Please select the type");
        err = 1;
    }
    if (cat_id == "0") {
        toastr.warning("Please Select Category");
        err = 1;
    }
    if (q_type == "") {
        toastr.warning("Please Select Quote Type");
        err = 1;
    } else if (q_type == "bgcolor") {
        if (bgcolor == "#000000") {

            toastr.warning("Please Select BG Color");
            err = 1;
        }
    } else if (q_type == "image") {
        if (bg_image == "") {
            toastr.warning("Please Select BG Image");
            err = 1;
        }
    }
    if (quote == "") {
        toastr.warning("Please Fill Quote");
        err = 1;
    }
    if (err == 1) {
        return false;
    } else {
        html2canvas([document.getElementById('parent')], {
            onrendered: function (canvas) {
                var imagedata = canvas.toDataURL('image/png');
                var imgdata = imagedata.replace(/^data:image\/(png|jpg);base64,/, "");
                //ajax call to save image inside folder
                $.ajax({
                    url: 'admin.php/quotes/create_quote',
                    data: {imgdata: imgdata, cat_id: cat_id, q_type: q_type, quote: quote, bgcolor: bgcolor, quote_type: quote_type},
                    type: 'post',
                    success: function (response) {
                        if (response == 0) {
                            $('.last_quote').hide();
                            toastr.warning("Quote Already Exist!!");
                        } else {
                            $('.last_quote').show();
                            $('.last_post').html(response);
//                            var html = 'Quotes which are published today on this '+ $('#cat_id').val();
//                            $('.last_post').html(html);
                            toastr.success("Quote Added Successfully!!");
                            $('html, body').animate({
                                scrollTop: $(".last_quote").offset().top
                            }, 2000);
                            window.setTimeout(function () {
                                location.href = "admin.php/quotes/quote_list";
                            }, 10000);
                        }

                    }
                });
            }
        });
    }
    return false;
});
$('#bg_image').change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#child').css('background-image', 'url(' + e.target.result + ')');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function edit_cat(cat_id, cat_name) {
    $("#cat_name").val(cat_name);
    $("#cat_id").val(cat_id);
    $("#cat_form").attr("action", "admin.php/category/edit_category");
    $("#topboxx").removeClass("collapsed-box");
    $("#bodyform").css("display", "block");
    $("#cat_title").text("Edit Category");
    window.scrollTo(0, 0);
}
$(function () {
    $("a#quoteimage").click(function (e) {
        e.preventDefault();
        var href = this.href;
        $("img#quote_preview").attr("src", href);
    });
});