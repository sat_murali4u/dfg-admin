<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Lunchbox Notes</title>

        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="img/fav.png" type="image/png">
        <!-- Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" href="vendor/device-mockups/device-mockups.min.css">

        <!-- Theme CSS -->
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/new-age.min.css" rel="stylesheet">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top">
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-lg-4 col-sm-4">
                        <img src="img/logo.svg" class="img-responsive logo" />
                    </div>
                    <div class="col-md-5 pull-right social-icons-top">
                        <ul class="nav pull-right">
                            <li><a href="https://www.facebook.com/lunchboxnotesapp/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/notes_lunchbox"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/daily_lunchbox_note/"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>                            
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <!--<div class="header-content">-->
                        <div class="header-content-inner">
                            <h1 class="text-center main" style="display: block;">Receive Notes Relevant to your loved ones <br/>and Share it with them...</h1>
                            <!--<a href="#download" class="btn btn-outline btn-xl page-scroll">Start Now for Free!</a>-->
                        </div>
                        <!--</div>-->
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div class="header-col1">
                            <img src="img/cont2.png" class="img-responsive arrow1 hidden-xs">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="header-col2 sharing text-center">
                            <img src="img/cont2.png" class="img-responsive arrow2 hidden-xs">
                            <div class="row">
                                <div class="col-md-1 col-sm-1 pull-right"></div>
                                <div class="col-md-4 col-sm-4 pull-right">
                                    <span class="share-to">SHARE TO</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2 col-xs-offset-2">                                
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/speech-bubble.svg"><h4>SMS</h4></div>
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/mail.svg"><h4>Email</h4></div>
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/facebook-logo.svg"><h4>Facebook</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2 col-xs-offset-2">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/whatsapp-logo.svg"><h4 class="whatsapp">Whatsapp</h4></div>
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/hangouts.svg"><h4>Hangout</h4></div>
                                        <div class="col-md-4 col-lg-4 col-xs-3 col-sm-4"><img class="img-responsive" src="img/more.svg"><h4>More</h4></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div class="header-col3">
                            <ul class="share">
                                <li>DAD</li>
                                <li>MOTHER</li>
                                <li>SON</li>
                                <li>FRIEND</li>
                                <li>HUBBY</li>
                            </ul>
                        </div>
                    </div>
                    <!--                <div class="col-sm-5">
                                        <div class="device-container">
                                            <div class="device-mockup iphone6_plus portrait white">
                                                <div class="device">
                                                    <div class="screen">
                                                         Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! 
                                                        <img src="img/demo-screen-1.jpg" class="img-responsive" alt="">
                                                    </div>
                                                    <div class="button">
                                                         You can hook the "home button" to some JavaScript events or just remove it 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                </div>
            </div>
        </header>
        <section id="features" class="features">
            <div class="container">               
                <div class="row">
                    <div class="col-md-4 col-sm-4">                        
                        <div class="screen">                           
                            <img src="img/T1.png" class="img-responsive" alt=""> </div>

                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="section-heading">
                                    <h2>Get a Lunch Box Notes for your loved ones and share it with them</h2>
                                    <p>"When you understand you need to spend whatever remains of your existence with some individual, you need whatever remains of your life to begin as quickly as time permits."</p><br>
                                    <p>"To laugh often and much; to win the respect of intelligent people and the affection of children... to leave the world a better place… to know even one life has breathed easier because you have lived. This is to have succeeded."</p>                     
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <ul class="feature-item">
                                        <li class="creative">
                                            <h3>Creative Design</h3>
                                            <p>"There are three responses to a piece of design – yes, no, and WOW! Wow is the one to aim for."</p></li>
                                        <li class="simple">                                            
                                            <h3>Simple</h3>
                                            <p>"Simple can be harder than complex: You have to work hard to get your thinking clean to make it simple. But it’s worth it in the end because once you get there, you can move mountains."</p>
                                        </li>
                                    </ul>
                                </div>
                                <!--                                <div class="col-md-6">
                                                                    <div class="feature-item">
                                                                        <i class="fa fa-paper-plane-o text-primary"></i>
                                                                        <h3>Simple</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                                                                    </div>
                                                                </div>-->
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="download" class="download text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <h2 class="section-heading">Download</h2>                       
                        <!--                        <div class="badges">
                                                    <a class="badge-link" href="#"><img src="img/google-play-badge.svg" alt=""></a>
                                                    <a class="badge-link" href="#"><img src="img/app-store-badge.svg" alt=""></a>
                                                </div>-->
                        <div class="row">
                            <div class="col-md-4 col-sm-4 apps">
                                <a href="javascript:void(0);" class="app-download"><img src="img/app store.png" class="img-responsive" /></a>
                            </div>
                            <div class="col-md-4 col-sm-4 apps">
                                <a href="javascript:void(0);" class="app-download"><img src="img/goole_play.png" class="img-responsive" /></a>
                            </div>
                            <div class="col-md-4 col-sm-4 windows">
                                <a href="javascript:void(0);" class="app-download"><img src="img/windows.png" class="img-responsive" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="screens text-center">
            <!--<div class="cta-content">-->
            <div class="container">
                <div class="row">
                    <h2>Screens</h2>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <!--                            <div class="well"> -->
                        <div id="myCarousel" class="carousel slide">

                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <!--<li data-target="#myCarousel" data-slide-to="2"></li>-->
                            </ol>

                            <!-- Carousel items -->
                            <div class="carousel-inner">

                                <div class="item active">
                                    <div class="row-fluid">
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S1.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S2.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S3.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S4.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                    </div><!--/row-fluid-->
                                </div><!--/item-->

                                <div class="item">
                                    <div class="row-fluid">
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S5.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S6.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><a href="javascript:void(0);" class=""><img src="img/S7.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                        <!--<div class="col-md-3"><a href="#" class=""><img src="img/T1.jpg" alt="Image" style="max-width:100%;" /></a></div>-->
                                    </div><!--/row-fluid-->
                                </div><!--/item-->

                                <!--                                <div class="item">
                                                                    <div class="row-fluid">
                                                                        <div class="col-md-3"><a href="#" class=""><img src="img/T2.jpg" alt="Image" style="max-width:100%;" /></a></div>
                                
                                                                    </div>/row-fluid
                                                                </div>/item-->

                            </div>
                        </div>

                    </div><!--/well-->   
                </div>
            </div>
            <!--</div>-->
            <!--</div>-->

        </section>
        <section class="contact text-center">
            <!--<div class="cta-content">-->
            <div class="container">


                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="row">
                            <h2 class="section-heading">Contact Us</h2>
                            <form method="post" action="index.php" role="form">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Name" name="name" class="form-control input-lg" id="name" required="Enter your name">
                                        <i class="fa fa-user glyphicon form-control-feedback"></i>
                                        <p class='text-danger' id="errName"></p>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Email" name="email" class="form-control input-lg" id="email" required="Enter your email">
                                        <i class="fa fa-envelope-o glyphicon form-control-feedback"></i>
                                        <p class='text-danger' id="errMail"></p>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <textarea class="form-control input-lg" rows="8" name="message" id="message" placeholder="Message" required="Enter your message"></textarea>
                                        <i class="fa fa-commenting-o glyphicon form-control-feedback"></i>
                                        <p class='text-danger' id="errMessage"></p>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="text-center col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <input type="submit" name="submit" class="btn btn-secondary btn-lg" id="submit" value="Send">
                                    </div>
                                </div>
                                <div class="col-sm-10 col-md-10 col-md-offset-2 col-sm-offset-2">
                                    <div class="form-group">                                    
                                        <?php echo $result; ?>	
                                    </div>
                                </div>                                
                            </form>
                            <p class="copy-text">&copy; Copyright 2017 . All Rights Reserved.</p>
                            <ul class="list-inline">
                                <li>
                                    <a href="#">Privacy |</a>
                                </li>
                                <li>
                                    <a href="#">Term of use</a>
                                </li>                    
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!--        <section id="contact" class="contact bg-primary">
                    <div class="container">
                        <h2>We <i class="fa fa-heart"></i> new friends!</h2>
                        <ul class="list-inline list-social">
                            <li class="social-twitter">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li class="social-facebook">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="social-google-plus">
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </li>
                        </ul>
                    </div>
                </section>-->

        <!--        <footer>
                    <div class="container">
                        <p>&copy; Copyright 2017 . All Rights Reserved.</p>
                        <ul class="list-inline">
                            <li>
                                <a href="#">Privacy</a>
                            </li>
                            <li>
                                <a href="#">Term of use</a>
                            </li>                    
                        </ul>
                    </div>
                </footer>-->

        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

        <!-- Theme JavaScript -->
        <script src="js/new-age.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#myCarousel').carousel({
                    interval: 5000
                })
            });
        </script>
        <?php
        if (isset($_POST["submit"])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $from = 'Lunchbox Notes';
            $to = 'shivazcodia@gmail.com';
            $subject = 'Message from Lunchbox Notes Landing page Contact Form ';

            $body = "From: $name\nE-Mail: $email\nMessage:\n $message";

            // Check if name has been entered
            if (!$_POST['name']) {
                $errName = 'Please enter your name';
            }

            // Check if email has been entered and is valid
            if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errEmail = 'Please enter a valid email address';
            }

            //Check if message has been entered
            if (!$_POST['message']) {
                $errMessage = 'Please enter your message';
            }
// If there are no errors, send the email
            if (!$errName && !$errEmail && !$errMessage) {
                if (mail($to, $subject, $body, $from)) {
                    $result = '<div class="alert alert-success">Thank You! I will be in touch</div>';
                } else {
                    $result = '<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
                }
            }
        }
        ?>

    </body>

</html>

