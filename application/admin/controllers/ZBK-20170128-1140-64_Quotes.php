<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotes extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library(array('tank_auth', "table"));
        $this->lang->load('tank_auth');
        $this->load->model('common_model');
        date_default_timezone_set('Asia/Kolkata');
        $this->data['notification'] = $this->notification->notification_list();
        if (!$this->tank_auth->is_logged_in()) {         // logged in
            redirect('auth/login');
        }
    }

    public function index() {
        $details = $this->common_model->select("*", 'qod_category');
        $data['options'] = [];
        foreach ($details as $value) {
            $data['options'][$value['cat_id']] = $value['cat_name'];
        }        
        $this->data['options'] = $data['options'];        
        $this->load->view("template/header", $this->data);
        $this->load->view("quotes_upload");
        $this->load->view("template/footer");
    }

    function create_quote() {
        $post = $this->input->post();
        $this->form_validation->set_rules('imgdata', 'imgdata', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cat_id', 'cat_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('q_type', 'q_type', 'trim|required|xss_clean');
        if ($post['q_type'] == "bgcolor") {
            $this->form_validation->set_rules('bgcolor', 'bgcolor', 'trim|required|xss_clean');
        }
        $this->form_validation->set_rules('quote', 'quote', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $where1 = array("qq_category_id ='" . $post['cat_id'] . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
            $count = $this->common_model->select('*', "qod_quotes", $where1);
            $imagedata = base64_decode($_POST['imgdata']);
            $filename = md5(uniqid(rand(), true));
            //path where you want to upload image
            $file = 'uploads/admin/quotes/' . $filename . '.jpg';
            $imageurl = baseURL . "uploads/admin/quotes/" . $filename . '.jpg';
            file_put_contents($file, $imagedata);
            $quote_code = str_replace(" ", "", $post['quote']);
            $quote_code = trim($quote_code, '"');
            $quote_code = strtolower($quote_code);
            $there = $this->common_model->select("*", "qod_quotes", array('qq_quote_code LIKE "%' . $quote_code . '%"'));
            if ($there) {
                echo 0;
                die;
            }
            $insert_data = array(
                "qq_type" => $post['q_type'],
                "qq_text" => $post['quote'],
                "qq_quote_code" => $quote_code,
                "qq_bg_image" => $filename . ".jpg",
                "qq_category_id" => $post['cat_id'],
                "qq_count_quotes" => $count != "" ? count($count) + 1 : 1,
                "qq_added_on" => date("y-m-d H:i:s")
            );
            if (isset($post['q_type']) && $post['q_type'] != "") {
                $insert_data['qq_bg_color'] = $post['q_type'];
            }
            $quote_id = $this->common_model->insert("qod_quotes", $insert_data);
            if ($quote_id) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }

    function quote_list() {
        $join1 = array(
            "table" => "qod_category",
            "join_string" => "qod_category.cat_id=qod_quotes.qq_category_id"
        );
        $join = array(
            $join1
        );
        $quote_list = $this->common_model->join('qod_quotes.qq_id, qod_quotes.qq_name,qod_quotes.qq_bg_image, qod_quotes.qq_text, qod_category.cat_name, qod_quotes.qq_added_on', 'qod_quotes', '', '', $join);
        if (!empty($quote_list)) {
            $i = 1;
            foreach ($quote_list as $key => $value) {
                $quote_list1[$key]['qq_id'] = $i;
//                $quote_list[$key]['qq_name'] = $value['qq_name'];
                $quote_list1[$key]['qq_text'] = '<a id="quoteimage"  href="uploads/admin/quotes/' . $value['qq_bg_image'] . '">'.$value['qq_text'].'</a>';
                $quote_list1[$key]['cat_name'] = $value['cat_name'];
                $quote_list1[$key]['qq_added_on'] = $value['qq_added_on'];
//                $quote_list1[$key]['qq_bg_image'] = '<a id="quoteimage"  href="uploads/admin/quotes/' . $value['qq_bg_image'] . '">View Quote</a>';
                $i++;
            }
            $this->table->set_heading("Quote ID", 'Quote Text', 'Quote Category', 'Created On');
            $data['table'] = $this->table_generate($quote_list1);
            $this->data['table'] = $data['table'];
        }
        $this->load->view('template/header', $this->data);
        $this->load->view('quote_list');
        $this->load->view('template/footer');
    }

    function table_generate($array) {
        $tmpl = array(
            'table_open' => '<table id="example2" class="table table-hover table-responsive">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        return $res = $this->table->generate($array);
    }

}
