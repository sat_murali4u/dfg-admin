<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library(array('tank_auth', "table"));
        $this->lang->load('tank_auth');
        $this->load->model('common_model');
        date_default_timezone_set('Asia/Kolkata');
        $this->data['notification'] = $this->notification->notification_list();
        if (!$this->tank_auth->is_logged_in()) {         // logged in
            redirect('auth/login');
        }
    }

    function index() {
        $category_list = $this->common_model->select("*", 'qod_category');
        $i = 1;
        foreach ($category_list as $key => $value) {
            $cat_name = "'" . $value['cat_name'] . "'";
            $category[$key]['s.no'] = $i;
            $category[$key]['cat_name'] = $value['cat_name'];
            $category[$key]['added_on'] = date("F j, Y, g:i:s a", strtotime($value['cat_added_on']));
            $category[$key]['action'] = '<a onclick="edit_cat(' . $value['cat_id'] . ',' . $cat_name . ')" class="btn btn-block btn-success btn-sm">Edit</button>';
            $i++;
        }
        $this->table->set_heading("S.No", 'Category Name', 'Created On', 'Action');
        $data['table'] = $this->table_generate($category);
        $this->data['table'] = $data['table'];
        $this->load->view('template/header', $this->data);
        $this->load->view('category');
        $this->load->view('template/footer');
    }

    function table_generate($array) {
        $tmpl = array(
            'table_open' => '<table id = "example2" class = "table table-hover table-responsive">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        return $res = $this->table->generate($array);
    }

    function create_category() {
        $post = $this->input->post();
        $this->form_validation->set_rules('cat_name', 'Category', 'trim|required|xss_clean');
        $there = $this->common_model->select("*", "qod_category", array('cat_name LIKE "%' . $post['cat_name'] . '%"'));
        if ($there) {
            $this->form_validation->set_rules('cat_name_unique', 'Category Unique', 'trim|required|xss_clean');
        }
        if ($this->form_validation->run()) {
            $data = array(
                "cat_name" => trim($post['cat_name']),
                "cat_added_on" => date('Y-m-d H:i:s')
            );
            $done = $this->common_model->insert("qod_category", $data);

            if ($done) {
                $this->session->set_flashdata('success_msg', 'Category ' . $post['cat_name'] . 'Created successfully!');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Category Name Already Exist!!');
        }
        redirect('category', 'refresh');
    }

    function edit_category() {
        $post = $this->input->post();
        $this->form_validation->set_rules('cat_name', 'Category', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cat_id', 'Category', 'trim|required|xss_clean');
        $there = $this->common_model->select("*", "qod_category", array('cat_name LIKE "%' . $post['cat_name'] . '%"'));
        if ($there) {
            $this->form_validation->set_rules('cat_name_unique', 'Category Unique', 'trim|required|xss_clean');
        }
        if ($this->form_validation->run()) {
            $data = array(
                "cat_name" => trim($post['cat_name']),
                "cat_updated_on" => date('Y-m-d H:i:s')
            );
            $done = $this->common_model->update(array("cat_id='" . $post['cat_id'] . "'"), $data, "qod_category");
            if ($done) {
                $this->session->set_flashdata('success_msg', 'Category Edited successfully!');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Category Name Already Exist!!');
        }
        redirect('category', 'refresh');
    }

}
