<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library(array('tank_auth', "table"));
        $this->lang->load('tank_auth');
        $this->load->model('common_model');
        date_default_timezone_set('Asia/Kolkata');
        $this->data['notification'] = $this->notification->notification_list();
        if (!$this->tank_auth->is_logged_in()) {         // logged in
            redirect('auth/login');
        }
    }

    function index() {
        $category_list = $this->common_model->select("*", 'qod_category');
        $i = 1;
        foreach ($category_list as $key => $value) {
            $sql = "SELECT MAX( counts ) as max  FROM (SELECT COUNT( `sub_user_id` ) AS counts, `sub_user_id` FROM qod_subscriptions WHERE `sub_category_id` = '" . $value['cat_id'] . "' AND  `sub_end_date` >  '" . date('Y-m-d H:i:s') . "' GROUP BY `sub_user_id`) AS s";
            $max_count = $this->common_model->normal_query($sql);
            $where = array("qq_category_id='" . $value['cat_id'] . "'", "DATE(qq_added_on) ='" . date("Y-m-d") . "'");
            $today_post = $this->common_model->select("count(qq_id) as total", 'qod_quotes', $where);
            $remain = $max_count[0]['max'] - $today_post[0]['total'];
            $category[$key]['s.no'] = $i;
            $category[$key]['cat_name'] = $value['cat_name'];
            $category[$key]['max_count'] = $max_count[0]['max'] == "" ? 0 : $max_count[0]['max'];
            $category[$key]['today_post'] = $today_post[0]['total'];
            $category[$key]['remain_post'] = $remain > 1 ? $remain : 0;
            $i++;
        }
        $this->table->set_heading("S.No", "Category", "Total subcribers", "Today's post", "Pending");
        $data['table'] = $this->table_generate($category);
        $this->data['table'] = $data['table'];
        $this->load->view('template/header', $this->data);
        $this->load->view('subscription_list');
        $this->load->view('template/footer');
    }

    function table_generate($array) {
        $tmpl = array(
            'table_open' => '<table id = "example2" class = "table table-hover table-responsive">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        return $res = $this->table->generate($array);
    }

}
