<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library(array('tank_auth', "table"));
        $this->lang->load('tank_auth');
        $this->load->model('common_model');
        date_default_timezone_set('Asia/Kolkata');
        $this->data['notification'] = $this->notification->notification_list();
        if (!$this->tank_auth->is_logged_in()) {         // logged in
            redirect('auth/login');
        }
    }

    function index() {    
        $filter = $this->common_model->normal_query("SELECT id, username, email, created, activated,(select COUNT(sub_user_id) from qod_subscriptions where sub_user_id = id) as subscriptions FROM `users`");
        if (!empty($filter)) {
            $i = 1;
            foreach ($filter as $key => $value) {
                $user_list[$key]['Reg.No'] = $i;
                $user_list[$key]['username'] = $value['username'];
                $user_list[$key]['email'] = $value['email'];
                $user_list[$key]['subscriptions'] = isset($value['subscriptions']) ? $value['subscriptions'] : 0;
                $user_list[$key]['created'] = date("F j, Y, g:i:s a", strtotime($value['created']));
                if ($value['activated'] == 1) {
                    $user_list[$key]['action'] = '<a href="admin.php/user/deactivate?id=' . $value['id'] . '" class="btn btn-block btn-danger btn-sm">Deactivate</button>';
                } else {
                    $user_list[$key]['action'] = '<a href="admin.php/user/activate?id=' . $value['id'] . '" class="btn btn-block btn-success btn-sm">Activate</button>';
                }
                $i++;
            }
            $this->table->set_heading("S.No", 'User Name', 'Email', 'Subscriptions', 'Created On', "Action");
            $data['table'] = $this->table_generate($user_list);
            $this->data['table'] = $data['table'];
        }
        $this->load->view('template/header', $this->data);
        $this->load->view('user_list');
        $this->load->view('template/footer');
    }

    function table_generate($array) {
        $tmpl = array(
            'table_open' => '<table id="example2" class="table table-hover table-responsive">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        return $res = $this->table->generate($array);
    }

    public function deactivate() {
        $id = $this->input->get('id');
        if ($id) {
            $data = array(
                "activated" => 0,
                "modified" => date("Y-m-d H:i:s")
            );
            $done = $this->common_model->update(array("id='$id'"), $data, "users");
            if ($done) {
                $this->session->set_flashdata('success_msg', 'Deactivated successfully!');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Something Wrong');
        }
        redirect('user', 'refresh');
    }

    public function activate() {
        $id = $this->input->get('id');
        if ($id) {
            $data = array(
                "activated" => 1,
                "modified" => date("Y-m-d H:i:s")
            );
            $done = $this->common_model->update(array("id='$id'"), $data, "users");
            if ($done) {
                $this->session->set_flashdata('success_msg', 'Activated successfully!');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Something Wrong');
        }
        redirect('user', 'refresh');
    }

}
