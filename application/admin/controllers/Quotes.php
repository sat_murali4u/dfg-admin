<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotes extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library(array('tank_auth', "table"));
        $this->lang->load('tank_auth');
        $this->load->model('common_model');
        date_default_timezone_set('Asia/Kolkata');
        $this->data['notification'] = $this->notification->notification_list();
        if (!$this->tank_auth->is_logged_in()) {         // logged in
            redirect('auth/login');
        }
    }

    public function index() {
//        print_r("SDf");die;
        $details = $this->common_model->select("*", 'qod_category');
        $data['options'] = array('0' => 'Select the category');
        foreach ($details as $value) {
            $sql = "SELECT MAX( counts ) as max  FROM (SELECT COUNT( `sub_user_id` ) AS counts, `sub_user_id` FROM qod_subscriptions WHERE `sub_category_id` = '" . $value['cat_id'] . "' AND  `sub_end_date` >  '" . date('Y-m-d H:i:s') . "' GROUP BY `sub_user_id`) AS s";
            $max_count = $this->common_model->normal_query($sql);
            $where = array("qq_category_id='" . $value['cat_id'] . "'", "DATE(qq_added_on) ='" . date("Y-m-d") . "'");
            $today_post = $this->common_model->select("count(qq_id) as total", 'qod_quotes', $where);
            $remain = $max_count[0]['max'] - $today_post[0]['total'];
            $details['remain_post'] = $remain >= 1 ? $remain : 0;
            $data['options'][$value['cat_id']] = $value['cat_name'] . ' (' . $details['remain_post'] . ')';
        }
        $this->data['options'] = $data['options'];
        $this->load->view("template/header", $this->data);
        $this->load->view("quotes_upload");
        $this->load->view("template/footer");
    }

    function create_quote() {
        $post = $this->input->post();
        $this->form_validation->set_rules('quote_type', 'quote_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('imgdata', 'imgdata', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cat_id', 'cat_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('q_type', 'q_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('textcolor', 'textcolor', 'trim|xss_clean');
        $this->form_validation->set_rules('qq_quote_date', 'qq_quote_date', 'trim|xss_clean');
        if ($post['q_type'] == "bgcolor") {
            $this->form_validation->set_rules('bgcolor', 'bgcolor', 'trim|required|xss_clean');
        }
        $this->form_validation->set_rules('quote', 'quote', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $where1 = array("qq_category_id ='" . $post['cat_id'] . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
            $count = $this->common_model->select('*', "qod_quotes", $where1);
            $imagedata = base64_decode($_POST['imgdata']);
            $filename = md5(uniqid(rand(), true));
            //path where you want to upload image
            $file = 'uploads/admin/quotes/' . $filename . '.jpg';
            $imageurl = baseURL . "uploads/admin/quotes/" . $filename . '.jpg';
            file_put_contents($file, $imagedata);
            $quote_code = strtolower(trim(str_replace(" ", "", $post['quote'])));
            $quote_code = trim($quote_code, '"');
            $there = $this->common_model->select("*", "qod_quotes", array('qq_quote_code LIKE "%' . $quote_code . '%"'));
            if ($there) {
                echo 0;
                die;
            }
            $insert_data = array(
                "qq_pay_type" => $post['quote_type'],
                "qq_type" => $post['q_type'],
                "qq_text" => $post['quote'],
                "qq_quote_code" => $quote_code,
                "qq_bg_image" => $filename . ".jpg",
                "qq_text_color" => $post['textcolor'],
                "qq_category_id" => $post['cat_id'],
                "qq_quote_date" => $post['qq_quote_date'],
                "qq_count_quotes" => $count != "" ? count($count) + 1 : 1,
                "qq_added_on" => date("y-m-d H:i:s")
            );

            if (isset($post['q_type']) && $post['q_type'] != "") {
                $insert_data['qq_bg_color'] = $post['bgcolor'];
            }
            $quote_id = $this->common_model->insert("qod_quotes", $insert_data);
            if ($quote_id) {
//                $joins = array(
//                    "table" => "qod_category",
//                    "join_string" => "qod_category.cat_id=qod_quotes.qq_category_id"
//                );
//                $join = array(
//                    $joins
//                );
//                $where = array("qq_category_id ='" . $post['cat_id'] . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
//                $get_today = $this->common_model->join('qod_quotes.qq_id, qod_quotes.qq_text, qod_category.cat_name, qod_quotes.qq_added_on', 'qod_quotes', $where, '', $join);
//                if (!empty($get_today)) {
//                    $i = 1;
//                    foreach ($get_today as $key => $value1) {
//                        $get_today1[$key]['qq_id'] = $i;
//                        $get_today1[$key]['qq_text'] = $value1['qq_text'];
//                        $get_today1[$key]['cat_name'] = $value1['cat_name'];
//                        $get_today1[$key]['qq_added_on'] = $value1['qq_added_on'];
//                        $i++;
//                    }
//                    $this->table->set_heading("Quote ID", 'Quote Text', 'Quote Category', 'Created On');
//                    $data['table'] = $this->table_generate($get_today1);
//                    $this->data['table'] = $data['table'];
//                    echo $a = $data['table'];
//                    die; //'<pre>'; print_r($this->data['table']); die;
//                }
                //  echo $this->load->view("quotes_upload", $this->data['table']);
                echo 1;
                die;
            } else {
                echo 2;
            }
        }
    }

    function quote_list() {
        $join1 = array(
            "table" => "qod_category",
            "join_string" => "qod_category.cat_id=qod_quotes.qq_category_id"
        );
        $join = array(
            $join1
        );
        $quote_list = $this->common_model->join('qod_quotes.qq_id,qod_quotes.qq_quote_date, qod_quotes.qq_name,qod_quotes.qq_bg_image, qod_quotes.qq_text, qod_category.cat_name, qod_quotes.qq_pay_type, qod_quotes.qq_added_on', 'qod_quotes', array("qod_quotes.qq_status='1'"), '', "", $join);
        if (!empty($quote_list)) {
            $i = 1;
            foreach ($quote_list as $key => $value) {
                $quote_list1[$key]['qq_id'] = $i;
//                $quote_list[$key]['qq_name'] = $value['qq_name'];
                $quote_list1[$key]['qq_text'] = '<a id="quoteimage"  href="uploads/admin/quotes/' . $value['qq_bg_image'] . '" data-toggle="tooltip" title = "' . trim($value['qq_text'], '"') . '!">' . substr($value['qq_text'], 0, 50) . "..." . '</a>';
                $quote_list1[$key]['cat_name'] = $value['cat_name'];
                $quote_list1[$key]['qq_pay_type'] = ($value['qq_pay_type'] == 0 ? 'General' : 'Paid');
                $quote_list1[$key]['qq_quote_date'] = $value['qq_quote_date'];
                $quote_list1[$key]['qq_added_on'] = $value['qq_added_on'];
                $quote_list1[$key]['action'] = '<a  href="admin.php/quotes/edit_quote/' . $value['qq_id'] . '">Edit</a> | <a  href="admin.php/quotes/delete_quote/' . $value['qq_id'] . '">Delete</a>';
                $i++;
            }
            $this->table->set_heading("Quote ID", 'Quote Text', 'Quote Category', 'Quote Type', 'Quote Day', 'Created On', 'Action');
            $data['table'] = $this->table_generate($quote_list1);
            $this->data['table'] = $data['table'];
        }
        $this->load->view('template/header', $this->data);
        $this->load->view('quote_list');
        $this->load->view('template/footer');
    }

    function get_lastpost() {
        $cat_id = $this->input->post('cat_id');
        $sql = "SELECT `qq_added_on` FROM `qod_quotes` WHERE `qq_category_id` = '" . $cat_id . "' ORDER BY `qq_id` DESC LIMIT 1";
        $last_post = $this->common_model->normal_query($sql);
        if (!empty($last_post)) {
            $last_posted = date_create($last_post[0]['qq_added_on']);
            echo date_format($last_posted, 'jS F Y G:i A');
            die;
        } else {
            echo 0;
            die;
        }
    }

    function get_todaysquote() {
        $cat_id = $this->input->post('cat_id');
        $joins = array(
            "table" => "qod_category",
            "join_string" => "qod_category.cat_id=qod_quotes.qq_category_id"
        );
        $join = array(
            $joins
        );
        $where = array("qq_category_id ='" . $cat_id . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
        $get_today = $this->common_model->join('qod_quotes.qq_id, qod_quotes.qq_text, qod_category.cat_name, qod_quotes.qq_added_on', 'qod_quotes', $where, '',"", $join);
        if (!empty($get_today)) {
            $i = 1;
            foreach ($get_today as $key => $value1) {
                $get_today1[$key]['qq_id'] = $i;
                $get_today1[$key]['qq_text'] = $value1['qq_text'];
                $get_today1[$key]['cat_name'] = $value1['cat_name'];
                $get_today1[$key]['qq_added_on'] = $value1['qq_added_on'];
                $i++;
            }
            $this->table->set_heading("Quote ID", 'Quote Text', 'Quote Category', 'Created On');
            $data['table'] = $this->table_generate($get_today1);
            $this->data['table'] = $data['table'];
            echo $a = $data['table'];
            die; //'<pre>'; print_r($this->data['table']); die;
        }
    }

    function table_generate($array) {
        $tmpl = array(
            'table_open' => '<table id="example2" class="table table-hover table-responsive">',
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            'table_close' => '</table>'
        );
        $this->table->set_template($tmpl);
        return $res = $this->table->generate($array);
    }

    function edit_quote() {
        $q_id = $this->uri->segment(3);
        if ($q_id) {
            $this->data['quote_details'] = $this->common_model->select("*", 'qod_quotes', array("qq_id='" . $q_id . "'"));
            $details = $this->common_model->select("*", 'qod_category');
            $data['options'] = array('0' => 'Select the category');
            foreach ($details as $value) {
                $sql = "SELECT MAX( counts ) as max  FROM (SELECT COUNT( `sub_user_id` ) AS counts, `sub_user_id` FROM qod_subscriptions WHERE `sub_category_id` = '" . $value['cat_id'] . "' AND  `sub_end_date` >  '" . date('Y-m-d H:i:s') . "' GROUP BY `sub_user_id`) AS s";
                $max_count = $this->common_model->normal_query($sql);
                $where = array("qq_category_id='" . $value['cat_id'] . "'", "DATE(qq_added_on) ='" . date("Y-m-d") . "'");
                $today_post = $this->common_model->select("count(qq_id) as total", 'qod_quotes', $where);
                $remain = $max_count[0]['max'] - $today_post[0]['total'];
                $details['remain_post'] = $remain >= 1 ? $remain : 0;
                $data['options'][$value['cat_id']] = $value['cat_name'] . ' (' . $details['remain_post'] . ')';
            }
            $this->data['options'] = $data['options'];
            $this->load->view("template/header", $this->data);
            $this->load->view("quotes_upload");
            $this->load->view("template/footer");
        } else {
            $this->session->set_flashdata('error_msg', 'Something Wrong');
            redirect('quotes/quote_list', 'refresh');
        }
    }

    function delete_quote() {
        $q_id = $this->uri->segment(3);
        $update_data = array(
            "qq_status" => 0,
            "qq_updated_on" => date('Y-m-d H:i:s'),
        );
        if ($q_id) {
            $done = $this->common_model->update(array("qq_id ='" . $q_id . "'"), $update_data, "qod_quotes");
            if ($done) {
                $this->session->set_flashdata('success_msg', 'Delected successfully!');
            } else {
                $this->session->set_flashdata('error_msg', 'Failed');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Something Wrong');
        }
        redirect('quotes/quote_list', 'refresh');
    }

    function update_quote() {
        $post = $this->input->post();
        $this->form_validation->set_rules('quote_type', 'quote_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('quote_id', 'quote_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('imgdata', 'imgdata', 'trim|required|xss_clean');
        $this->form_validation->set_rules('cat_id', 'cat_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('q_type', 'q_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('textcolor', 'textcolor', 'trim|xss_clean');
        $this->form_validation->set_rules('qq_quote_date', 'qq_quote_date', 'trim|xss_clean');
        if ($post['q_type'] == "bgcolor") {
            $this->form_validation->set_rules('bgcolor', 'bgcolor', 'trim|required|xss_clean');
        }
        $this->form_validation->set_rules('quote', 'quote', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $where1 = array("qq_category_id ='" . $post['cat_id'] . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
            $count = $this->common_model->select('*', "qod_quotes", $where1);
            $imagedata = base64_decode($_POST['imgdata']);
            $filename = md5(uniqid(rand(), true));
            //path where you want to upload image
            $file = 'uploads/admin/quotes/' . $filename . '.jpg';
            $imageurl = baseURL . "uploads/admin/quotes/" . $filename . '.jpg';
            file_put_contents($file, $imagedata);
            $quote_code = strtolower(trim(str_replace(" ", "", $post['quote'])));
            $quote_code = trim($quote_code, '"');
            $there = $this->common_model->select("*", "qod_quotes", array('qq_quote_code LIKE "%' . $quote_code . '%"', "qq_id NOT IN ('" . $post['quote_id'] . "')"));
            if ($there) {
                echo 0;
                die;
            }
            $insert_data = array(
                "qq_pay_type" => $post['quote_type'],
                "qq_type" => $post['q_type'],
                "qq_text" => $post['quote'],
                "qq_quote_code" => $quote_code,
                "qq_bg_image" => $filename . ".jpg",
                "qq_text_color" => $post['textcolor'],
                "qq_category_id" => $post['cat_id'],
                "qq_quote_date" => $post['qq_quote_date'],
                "qq_updated_on" => date("y-m-d H:i:s")
            );

            if (isset($post['q_type']) && $post['q_type'] != "") {
                $insert_data['qq_bg_color'] = $post['bgcolor'];
            }
            $quote_id = $this->common_model->update(array("qq_id='" . $post['quote_id'] . "'"), $insert_data, "qod_quotes");
            if ($quote_id) {
//                $joins = array(
//                    "table" => "qod_category",
//                    "join_string" => "qod_category.cat_id=qod_quotes.qq_category_id"
//                );
//                $join = array(
//                    $joins
//                );
//                $where = array("qq_category_id ='" . $post['cat_id'] . "'", "MONTH(qq_added_on)='" . date('m') . "'", "YEAR(qq_added_on)='" . date('Y') . "'", "DAY(qq_added_on)='" . date('d') . "'");
//                $get_today = $this->common_model->join('qod_quotes.qq_id, qod_quotes.qq_text, qod_category.cat_name, qod_quotes.qq_added_on', 'qod_quotes', $where, '', $join);
//                if (!empty($get_today)) {
//                    $i = 1;
//                    foreach ($get_today as $key => $value1) {
//                        $get_today1[$key]['qq_id'] = $i;
//                        $get_today1[$key]['qq_text'] = $value1['qq_text'];
//                        $get_today1[$key]['cat_name'] = $value1['cat_name'];
//                        $get_today1[$key]['qq_added_on'] = $value1['qq_added_on'];
//                        $i++;
//                    }
//                    $this->table->set_heading("Quote ID", 'Quote Text', 'Quote Category', 'Created On');
//                    $data['table'] = $this->table_generate($get_today1);
//                    $this->data['table'] = $data['table'];
//                    echo $a = $data['table'];
//                    die; //'<pre>'; print_r($this->data['table']); die;
//                }
                //  echo $this->load->view("quotes_upload", $this->data['table']);
                echo 1;
                die;
            } else {
                echo 2;
            }
        }
    }

}
