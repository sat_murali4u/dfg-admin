<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_Model extends CI_Model {

    function __construct() {

// Call the Model constructor
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        if ($this->db->insert_id()) {
            return $insert_id = $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function delete($where, $table) {
        if (isset($where) && $where != "") {
            foreach ($where as $val) {
                $this->db->where($val);
            }
        }
        $this->db->delete($table);
        $afftectedRows = $this->db->affected_rows();
        if ($afftectedRows) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function select($select, $table, $where = FALSE, $order_by = FALSE) {
//        $where = array("student_id = '" . $data['student_id'] . "'", "stu_class_id = '" . $data['stu_class_id'] . "'", "attendance_date = '" . date('Y-m-d') . "'");
//        $check = $this->select("*", "student_attendance", $where);
        $this->db->select($select);
        $this->db->from($table);
        if (isset($where) && $where != "") {
            foreach ($where as $val) {
                $this->db->where($val);
            }
        }
        if (isset($where) && $where != "") {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $options = $query->result_array();
            return $options;
        } else {
            return false;
        }
    }

    function update($where, $data, $table) {
        if (isset($where) && $where != "") {
            foreach ($where as $val) {
                $this->db->where($val);
            }
        }
        $this->db->update($table, $data);
        $afftectedRows = $this->db->affected_rows();
        if ($afftectedRows > 0) {
            return 1;
        } else {
            return false;
        }
    }

    function join($select, $table, $where = FALSE, $order_by = FALSE, $join) {
        $this->db->select($select);
        $this->db->from($table);
        if (isset($join) && $join != "") {
            foreach ($join as $value) {
                $this->db->join($value['table'], $value['join_string']);
            }
        }
        if (isset($where) && $where != "") {
            foreach ($where as $val) {
                $this->db->where($val);
            }
        }
        if (isset($where) && $where != "") {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $options = $query->result_array();
            return $options;
        } else {
            return false;
        }
    }

    public function normal_query($sql) {
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $options = $query->result_array();
            return $options;
        } else {
            return false;
        }
    }

}
