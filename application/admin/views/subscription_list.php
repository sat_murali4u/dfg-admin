

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>Subscription List</h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Subscription List</li>

        </ol>

    </section>


    <section class="content">

        <div class="row">

            <div class="col-md-12 col-sm-12">

                <div class="box box-primary">
                    <div class="box-body">
                        <?php
                        if (isset($table)) {

                            echo $table;
                        } else {

                            echo 'No Subscription were found!';
                        }
                        ?>           

                    </div>
                </div>

            </div>

        </div>


    </section>

</div>

