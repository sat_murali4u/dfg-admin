<?php // echo '<pre>'; print_r($category); die;         ?>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>Quotes List</h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Quotes List</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-md-8 col-sm-8">

                <div class="box box-primary">
                    <div class="box-body">
                        <?php
                        if (isset($table)) {

                            echo $table;
                        } else {

                            echo 'No quotes were found!';
                        }
                        ?>           

                    </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-4">

                <div class="box box-primary">
                    <div class="box-header with-border">

                        <h3 class="box-title text-center">Quote Preview</h3>

                    </div>
                    <div class="box-body">
                        <div class="height500 quote_preview" id="parent">
                            <img src="http://qod.cloudzire.com/uploads/admin/quotes/preview.png" id="quote_preview" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.row -->

        <!-- Main row -->

    </section>

</div>

