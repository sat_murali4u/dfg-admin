<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Category Management</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Category Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="box box-primary" id="topboxx">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="cat_title">Create Category</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;" id="bodyform">
                        <div class="row">
                            <form action="admin.php/category/create_category" method="post" id="cat_form">
                                <div class="box-body text-center">
                                    <div class="col-md-12 form-group " >
                                        <div class="col-md-offset-3 col-md-6 form-group " >
                                            <label for="bgcolor">Category Name</label><br>
                                            <input class="form-control" required="" type="text" name="cat_name"  id="cat_name" value="<?php echo (set_value('cat_name') != '') ? set_value('cat_name') : ''; ?>">
                                            <input class="form-control" required="" type="hidden" name="cat_id"  id="cat_id" value="<?php echo (set_value('cat_id') != '') ? set_value('cat_id') : ''; ?>">
                                            <span class="text-red"><?php echo form_error('cat_name'); ?></span>
                                        </div> 
                                    </div> 
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary" >Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">

                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <?php
                        if (isset($table)) {

                            echo $table;
                        } else {

                            echo 'No Subscription were found!';
                        }
                        ?>           

                    </div>
                </div>

            </div>

        </div>


    </section>

</div>

