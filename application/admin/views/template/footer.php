<?php if (@$menu) { ?>
    <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="assets/js/toastr.js"></script>
<?php } else { ?>
    <footer class="main-footer">

        <div class="pull-right hidden-xs">

            <b>Version</b> 1.0

        </div>

        <strong>Copyright &copy; 2017 <a href="javascript:void(0);">DAM Pro Filename Creator</a>.</strong> All rights reserved.

    </footer><!-- jQuery 2.2.3 -->

    <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <!-- jQuery UI 1.11.4 -->

    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

    <script>

        $.widget.bridge('uibutton', $.ui.button);

    </script>

    <!-- Bootstrap 3.3.6 -->

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- Morris.js charts -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

                                                                                                                                                                            <!--<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>-->

    <!-- datepicker -->

    <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>

    <!-- Bootstrap WYSIHTML5 -->

    <script src="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    <!-- Slimscroll -->

    <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->

    <script src="assets/plugins/fastclick/fastclick.js"></script>

    <!-- AdminLTE App -->

    <script src="assets/dist/js/app.min.js"></script>

    <script src="assets/js/quote.js"></script>
    <script src="assets/js/push.js"></script>

    <script src="assets/js/html2canvas.js"></script>

    <script src="assets/js/toastr.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

                                                                                                                                                                            <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <script>
        $(function () {

            $("#example").DataTable();

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false

            });

        });
    </script>
<?php } ?>
<script>


    $(document).ready(function () {
        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', 'http://soundbible.com/grab.php?id=2158&type=mp3');
        audioElement.setAttribute('autoplay:false', 'autoplay');
        //audioElement.load code above. if you take out :false from the code the file will auto play than everythin works the same after that()
        $.get();
        audioElement.addEventListener("load", function () {
            audioElement.play();
        }, true);
        error();
        success();
        info();
        function error() {
            var message =<?php echo json_encode($this->session->flashdata('error_msg')); ?>;
            if (message) {
                toastr.error(message);
                audioElement.play();
            }
        }
        function success() {
            var message =<?php echo json_encode($this->session->flashdata('success_msg')); ?>;
            if (message) {
                toastr.success(message);
                audioElement.play();
            }
        }
        function info() {
            var message =<?php echo json_encode($this->session->flashdata('message')); ?>;
            if (message) {
                toastr.info(message);
                audioElement.play();
            }
        }
    });
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-3d',
        todayHighlight: true,
        startDate:'today'
    });
    
</script>




</body>

</html>

