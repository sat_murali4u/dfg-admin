<?php
$new_password = array(
    'name' => 'new_password',
    'id' => 'new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'class' => "input form-control",
    'data-type' => "password"
);
$confirm_new_password = array(
    'name' => 'confirm_new_password',
    'id' => 'confirm_new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'class' => "input form-control",
    'data-type' => "password"
);
?>
<style>    
    body {
        background-color: #222d32;
    }    
</style>
<div class="box box-primary qod" id="login-box">
    <div class="box-header with-border qod-title"><h3 class="box-title">Forgotten Password</h3></div>
    <?php echo form_open($this->uri->uri_string()); ?>                                      
    <div class="box-body">  
        <div class="form-group">
            <?php echo form_label('New Password', $new_password['id']); ?>
            <?php echo form_password($new_password); ?>
            <span style="color:red;"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>
        </div>
        <div class="group">
            <?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
            <?php echo form_password($confirm_new_password); ?>
            <span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>
        </div>
    </div>
    <div class="box-footer">
        <?php echo form_submit('change', 'Change Password', 'class="btn btn-block btn-primary"'); ?>
    </div>
<?php echo form_close(); ?>
</div>

