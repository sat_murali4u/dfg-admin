<?php
$login = array(
    'name' => 'login',
    'id' => 'login',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
    'class' => 'form-control',
);
if ($this->config->item('use_username', 'tank_auth')) {
    $login_label = 'Email or login';
} else {
    $login_label = 'Email';
}
?>
<style>    
    body {
        background-color: #222d32;
    }    
</style>
<div class="box box-primary qod" id="login-box">
    <div class="box-header with-border qod-title"><h3 class="box-title">Forgotten Password</h3></div>
    <?php echo form_open($this->uri->uri_string()); ?>
    <div class="box-body">  
        <div class="form-group">
            <?php echo form_label($login_label, $login['id']); ?>
            <?php echo form_input($login); ?>
            <span style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']]) ? $errors[$login['name']] : ''; ?></span>
        </div>
    </div>
    <div class="box-footer">
        <?php echo form_submit('reset', 'Get a new password', 'class="btn btn-block btn-primary"'); ?>
        <br>
        <?php echo anchor('/auth/login/', 'Back To Login', 'class="text-center forgot"'); ?>
    </div>
    <?php echo form_close(); ?>
</div>