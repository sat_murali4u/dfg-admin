<div class="content-wrapper">



    <!-- Content Header (Page header) -->



    <section class="content-header">



        <h1>



            <?php echo isset($quote_details) ? "Update A Quote" : "Add a New Quote"; ?> 



        </h1>



        <ol class="breadcrumb">



            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>



            <li class="active">Add a New Quote</li>



        </ol>



    </section>







    <section class="content">



        <div class="row">



            <div class="col-lg-8">



                <div class="box box-primary">





                    <!--<form role="form" lpformnum="1">-->



                    <div class="box-body">



                        <!--                            <div class="form-group">



                                                        <label for="name">Name</label>



                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">



                                                    </div>-->                        

                        <div class="col-lg-12">                            

                            <div><label>Quote Type:</label></div>

                            <div class="col-md-12 quote_type">

                                <input type="radio" class="quote_type " id="general" name="quote_type" <?php echo isset($quote_details[0]['qq_pay_type']) && $quote_details[0]['qq_pay_type'] == 0 ? "checked" : ""; ?> value="0">

                                <label for="general" class="col-md-6 col-sm-6 col-xs-6">Sample</label>

                                <input type="radio" class="quote_type " id="paid" name="quote_type" <?php echo isset($quote_details[0]['qq_pay_type']) && $quote_details[0]['qq_pay_type'] == 1 ? "checked" : ""; ?> value="1">

                                <label for="paid" class="col-md-6 col-sm-6 col-xs-6">Paid</label>                               

                            </div>



                        </div>

                        <div class="clearfix"></div>



                        <div class="row col-lg-12">

                            <div class="col-lg-6">

                                <div class="form-group">

                                    <label>Category</label>

                                    <?php
                                    $select = isset($quote_details) && $quote_details ? $quote_details[0]['qq_category_id'] : "";

                                    $js = array('class' => 'form-control', 'id' => 'cat_id', 'onchange' => 'last_quote_post(this.value)');

                                    echo form_dropdown('cat_id', $options, $select, $js);
                                    ?>

                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="form-group">

                                    <label>Select the Quote Day</label>

                                    <input type='text' name="qq_quote_date" id="qq_quote_date" class="datepicker form-control" value="<?php echo isset($quote_details[0]['qq_quote_date']) && $quote_details[0]['qq_quote_date'] ? $quote_details[0]['qq_quote_date'] : ""; ?>">

                                </div>

                            </div>

                        </div>



                        <div class="col-lg-12 form-group">



                            <label>Type</label>



                            <select class="form-control" id="q_type" name="type" onChange="quote_type(this.value);">



                                <option value="">Select the quote type</option>



                                <option  <?php echo isset($quote_details[0]['qq_type']) && $quote_details[0]['qq_type'] == "text" ? "selected" : ""; ?> value="text">Text only</option>



                                <option  <?php echo isset($quote_details[0]['qq_type']) && $quote_details[0]['qq_type'] == "bgcolor" ? "selected" : ""; ?> value="bgcolor">Text with background color</option>



                                <option  <?php echo isset($quote_details[0]['qq_type']) && $quote_details[0]['qq_type'] == "image" ? "selected" : ""; ?> value="image">Text with background image</option>                                    



                            </select>



                        </div>   







                        <div class="clearfix"></div>





                        <div class="col-lg-12">



                            <div class="form-group" id="Quotef">



                                <label for="quote">Quote</label>



                                <textarea class="form-control" name="quote" id="quote" rows="4"><?php echo isset($quote_details[0]['qq_text']) && $quote_details[0]['qq_text'] ? $quote_details[0]['qq_text'] : ""; ?></textarea>



                                <p id="last_posted"></p>



                            </div> 



                            <div class="col-lg-4 form-group">



                                <label for="bgcolor">Select Text Color</label><br>



                                <input class="form-group" type="color" name="textcolor" onchange="textcolorapply(this.value)" id="textcolor" value="<?php echo isset($quote_details[0]['qq_text_color']) && $quote_details[0]['qq_text_color'] ? $quote_details[0]['qq_text_color'] : ""; ?>">



                            </div>

                            <div class="col-lg-4 form-group <?php echo isset($quote_details[0]['qq_type']) && $quote_details[0]['qq_type'] == "bgcolor" ? "" : "disable"; ?>" id="bgf">



                                <label for="bgcolor">Select Background Color</label><br>



                                <input class="form-group" type="color" name="bgcolor" onchange="bgcolorapply(this.value)" id="bgcolor" value="<?php echo isset($quote_details[0]['qq_bg_color']) && $quote_details[0]['qq_bg_color'] ? $quote_details[0]['qq_bg_color'] : ""; ?>">



                            </div>                            



                            <div class="col-lg-4 form-group <?php echo isset($quote_details[0]['qq_type']) && $quote_details[0]['qq_type'] == "image" ? "" : "disable"; ?>" id="filef">



                                <label for="exampleInputFile">Select Background Image</label>



                                <input type="file" name="bg_image" id="bg_image">
                                <input type="hidden" name="quote_id" id="quote_id" value="<?php echo $this->uri->segment(3) != "" ? $this->uri->segment(3) : ""; ?>">



                            </div>  



                        </div>                        

                    </div>



                    <div class="box-footer text-center">



                        <button type="submit" class="btn btn-primary pull-right" id="<?php echo $this->uri->segment(2) == "edit_quote" ? "edit_quote" : "submit"; ?>">Submit</button>



                    </div>



                    <!--</form>-->



                </div> 



            </div>



            <div class="col-lg-4 col-xs-12">



                <div class="box box-primary overflowhide">



                    <div class="box-header with-border">



                        <h3 class="box-title text-center">Preview</h3>



                    </div>



                    <div class="height500" id="parent">
                        <?php if (isset($quote_details[0]['qq_bg_image']) && $quote_details[0]['qq_bg_image'] != "") { ?>
                            <div class="bgi"><img src="uploads/admin/quotes/<?php echo isset($quote_details[0]['qq_bg_image']) && $quote_details[0]['qq_bg_image'] != "" ? $quote_details[0]['qq_bg_image'] : ""; ?>"></div>
                        <?php } ?>
                        <div class="col-lg-12 text-center <?php echo isset($quote_details[0]['qq_bg_image']) && $quote_details[0]['qq_bg_image'] != "" ? "displaynone" : ""; ?>" id="child">



                            <h3 id="quotegen"> "Don't cry because it's over, smile because it happened." </h3>



                        </div>



                    </div>



                </div>



                <img src="" id="img_if">



            </div>



        </div>



        <div class="row">



            <div class="col-md-12 col-lg-12 col-sm-12 last_quote">



                <div class="box box-primary">

                    <h3>Today published quotes on this category</h3>

                    <div class="box-body last_post">



                    </div>

                </div>



            </div>

        </div>



    </section>



</div>







