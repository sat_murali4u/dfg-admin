<?php // echo '<pre>'; print_r($category); die;     ?>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>Users List</h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Users List</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-md-12 col-sm-12">

                <div class="box box-primary">
                    <div class="box-body">
                        <?php
                        if (isset($table)) {

                            echo $table;
                        } else {

                            echo 'No Users were found!';
                        }
                        ?>           

                    </div>
                </div>

            </div>

        </div>

        <!-- /.row -->

        <!-- Main row -->

    </section>

</div>

