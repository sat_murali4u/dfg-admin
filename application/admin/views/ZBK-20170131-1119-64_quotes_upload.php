<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Add a New Quote

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Add a New Quote</li>

        </ol>

    </section>



    <section class="content">

        <div class="row">

            <div class="col-lg-8">

                <div class="box box-primary">


                    <!--<form role="form" lpformnum="1">-->

                    <div class="box-body">

                        <!--                            <div class="form-group">

                                                        <label for="name">Name</label>

                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAPhJREFUOBHlU70KgzAQPlMhEvoQTg6OPoOjT+JWOnRqkUKHgqWP4OQbOPokTk6OTkVULNSLVc62oJmbIdzd95NcuGjX2/3YVI/Ts+t0WLE2ut5xsQ0O+90F6UxFjAI8qNcEGONia08e6MNONYwCS7EQAizLmtGUDEzTBNd1fxsYhjEBnHPQNG3KKTYV34F8ec/zwHEciOMYyrIE3/ehKAqIoggo9inGXKmFXwbyBkmSQJqmUNe15IRhCG3byphitm1/eUzDM4qR0TTNjEixGdAnSi3keS5vSk2UDKqqgizLqB4YzvassiKhGtZ/jDMtLOnHz7TE+yf8BaDZXA509yeBAAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;">

                                                    </div>-->                        
                        <div class="col-lg-12">                            
                            <div><label>Quote Type:</label></div>
                            <div class="col-md-12 quote_type">
                                <input type="radio" class="quote_type " id="general" name="quote_type" value="0">
                                <label for="general" class="col-md-6">General</label>
                                <input type="radio" class="quote_type " id="paid" name="quote_type" value="1">
                                <label for="paid" class="col-md-6">Paid</label>                               
                            </div>
                            <!--                            <div class="form-group quote_type">                                    
                                                            <input type="radio" name="quote_type" id="quote_type" value="0">
                                                            <label for="general">General</label>                                  
                                                        </div>-->
                        </div>
                        <!--                        <div class="col-md-6 col-sm-6">
                                                    <div class="form-group quote_type">                                    
                                                        <input type="radio" name="quote_type" id="quote_type" value="1">
                                                        <label for="paid">Paid</label>                                    
                                                    </div>
                                                </div>                       -->
                        <div class="clearfix"></div>
                        <div class="col-lg-12">
                            <div class="form-group">

                                <label>Category</label>

                                <?php
                                $js = array('class' => 'form-control', 'id' => 'cat_id', 'onchange' => 'last_quote_post(this.value)');
                                echo form_dropdown('cat_id', $options, '', $js);
                                ?>

                            </div>
                        </div>

                        <div class="col-lg-12 form-group">

                            <label>Type</label>

                            <select class="form-control" id="q_type" name="type" onChange="quote_type(this.value)">

                                <option value="">Select the quote type</option>

                                <option value="text">Text only</option>

                                <option value="bgcolor">Text with background color</option>

                                <option value="image">Text with background image</option>                                    

                            </select>

                        </div>   



                        <div class="clearfix"></div>


                        <div class="col-lg-12">

                            <div class="form-group" id="Quotef">

                                <label for="quote">Quote</label>

                                <textarea class="form-control" name="quote" id="quote" rows="4"></textarea>

                                <p id="last_posted"></p>

                            </div> 

                            <div class="col-lg-4 form-group">

                                <label for="bgcolor">Select Text Color</label><br>

                                <input class="form-group" type="color" name="textcolor" onchange="textcolorapply(this.value)" id="textcolor" >

                            </div>
                            <div class="col-lg-4 form-group disable" id="bgf">

                                <label for="bgcolor">Select Background Color</label><br>

                                <input class="form-group" type="color" name="bgcolor" onchange="bgcolorapply(this.value)" id="bgcolor" >

                            </div>                            

                            <div class="col-lg-4 form-group disable" id="filef">

                                <label for="exampleInputFile">Select Background Image</label>

                                <input type="file" name="bg_image" id="bg_image">

                            </div>  

                        </div>                        
                    </div>

                    <div class="box-footer text-center">

                        <button type="submit" class="btn btn-primary pull-right" id="submit">Submit</button>

                    </div>

                    <!--</form>-->

                </div> 

            </div>

            <div class="col-lg-4 col-xs-6">

                <div class="box box-primary overflowhide">

                    <div class="box-header with-border">

                        <h3 class="box-title text-center">Preview</h3>

                    </div>

                    <div class="height500" id="parent">
                        <div class="col-lg-12 text-center" id="child">

                            <h3 id="quotegen"> "Don't cry because it's over, smile because it happened." </h3>

                        </div>

                    </div>

                </div>

                <img src="" id="img_if">

            </div>

        </div>

        <div class="row">

            <div class="col-md-12 col-lg-12 col-sm-12 last_quote">

                <div class="box box-primary">
                    <div class="box-body last_post">
                        <h3>Today published quotes on this category</h3>
                    </div>
                </div>

            </div>
        </div>

    </section>

</div>



