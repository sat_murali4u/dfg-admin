<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Qod extends REST_Controller {

    function __construct() {
        parent::__construct();
//        $this->load->model(array(""));
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->load->helper('email');
        $this->load->helper('string');
        $this->load->model('common_model');
        define('STATUS', 'addstatus');
        define('MESSAGE', 'addmsg');
        $this->load->helper(array('url', 'language'));
    }

    function registor_post() {
        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');
        $data['errors'] = "";
        $email_activation = $this->config->item('email_activation', 'tank_auth');
        if ($this->form_validation->run()) {        // validation ok
            if (!is_null($data = $this->tank_auth->create_user(
                            $use_username ? $this->form_validation->set_value('username') : '', $this->form_validation->set_value('email'), $this->form_validation->set_value('password'), $email_activation, $this->input->post('timezone')))) {         // success
                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                if ($email_activation) {         // send "activate" email
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

//                    $this->_send_email('activate', $data['email'], $data);

                    unset($data['password']); // Clear password (just for any case)
                } else {
                    if ($this->config->item('email_account_details', 'tank_auth')) { // send "welcome" email
//                        $this->_send_email('welcome', $data['email'], $data);
                    }
                    unset($data['password']); // Clear password (just for any case)
                }
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'] = $this->lang->line($v);
            }
        }
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function login_post() {
        $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
                $this->config->item('use_username', 'tank_auth'));
        $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');
        $this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
// Get login for counting attempts to login
        if ($this->config->item('login_count_attempts', 'tank_auth') AND ( $login = $this->input->post('login'))) {
            $login = $this->security->xss_clean($login);
        } else {
            $login = '';
        }
        if ($this->form_validation->run()) {        // validation ok
            if ($this->tank_auth->login(
                            $this->form_validation->set_value('login'), $this->form_validation->set_value('password'), $this->form_validation->set_value('remember'), $data['login_by_username'], $data['login_by_email'])) {        // success
                $user_data = $this->common_model->select("*", "users", array("LOWER(email) ='" . strtolower($this->form_validation->set_value('login')) . "'"));
                unset($user_data[0]['password']);
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $user_data[0]), REST_Controller::HTTP_OK);
                die;
            } else {
                $errors = $this->tank_auth->get_error_message();
                if (isset($errors['banned'])) {        // banned user
                    $data['errors']['banned'] = $this->lang->line('auth_message_banned') . ' ' . $errors['banned'];
                } elseif (isset($errors['not_activated'])) {    // not activated user
                    $data['errors']['activate'] = "Please Activate your account and try again";
                } else {             // fail
                    foreach ($errors as $k => $v)
                        $data['errors'] = $this->lang->line($v);
                }
            }
        }
        unset($data['login_by_username']);
        unset($data['login_by_email']);
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function forgotpassword_post() {
        $this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');
        $data['errors'] = array();
        if ($this->form_validation->run()) {        // validation ok
            if (!is_null($data = $this->tank_auth->forgot_password(
                            $this->form_validation->set_value('login')))) {

                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

// Send email with password activation link
                $this->_send_email('forgot_password', $data['email'], $data);

                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $this->lang->line('auth_message_new_password_sent')), REST_Controller::HTTP_OK);
                die;
            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'] = $this->lang->line($v);
            }
        }
        unset($data['login_by_username']);
        unset($data['login_by_email']);
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function changepassword_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');
        $data['errors'] = '';
        if ($this->form_validation->run()) {        // validation ok
            if ($this->tank_auth->change_password(
                            $this->form_validation->set_value('old_password'), $this->form_validation->set_value('new_password'), $this->form_validation->set_value('user_id'))) { // success
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $this->lang->line('auth_message_password_changed')), REST_Controller::HTTP_OK);
                die;
            } else {              // fail
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'][$k] = $this->lang->line($v);
            }
        }
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function _send_email($type, $email, &$data) {
        $this->load->library('email');
        $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->to($email);
        $this->email->subject(sprintf($this->lang->line('auth_subject_' . $type), $this->config->item('website_name', 'tank_auth')));
        $this->email->message($this->load->view('email/' . $type . '-html', $data, TRUE));
        $this->email->set_alt_message($this->load->view('email/' . $type . '-txt', $data, TRUE));
        $this->email->send();
    }

    public function update_id_post() {
        $got = $this->buyerapi_model->select('*', "update_id");
        if ($got) {
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
        } else {
            header(STATUS . ': 0');
            header(MESSAGE . ': failed');
            $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function all_category_post() {
        $got = $this->common_model->select('*', "qod_category");
        if ($got) {
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
        } else {
            header(STATUS . ': 0');
            header(MESSAGE . ': failed');
            $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function create_subscription_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_nick_name', 'Nick Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_category_id', 'Catagory', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $where = array("sub_nick_name ='" . $post['sub_nick_name'] . "'");
            $got = $this->common_model->select('*', "qod_subscriptions", $where);
            if ($got) {
                $data['errors'] = "Nick Name Already Exiest";
            } else {
                $where1 = array("sub_category_id ='" . $post['sub_category_id'] . "'", "sub_user_id='" . $post['user_id'] . "'");
                $count = $this->common_model->select('*', "qod_subscriptions", $where1);
                $insert_data = array(
                    "sub_user_id" => $post['user_id'],
                    "sub_nick_name" => $post['sub_nick_name'],
                    "sub_category_id" => $post['sub_category_id'],
                    "sub_user_count" => $count != "" ? count($count) + 1 : 1,
                    "sub_start_date" => date('Y-m-d H:i:s'),
                    "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
                    "sub_added_on" => date('Y-m-d H:i:s'),
                );
                $sub_id = $this->common_model->insert("qod_subscriptions", $insert_data);
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $sub_id), REST_Controller::HTTP_OK);
                die;
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function subscription_list_post() {
        $data = "";
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('scroll_id', 'scroll_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            if ($post['scroll_id'] == 0 || $post['scroll_id'] == "") {
                $where = array("sub_user_id ='" . $post['user_id'] . "'");
            } else {
                $where = array("sub_user_id ='" . $post['user_id'] . "'", "sub_id <'" . $post['scroll_id'] . "'");
            }
            $join1 = array(
                "table" => "qod_category",
                "join_string" => "qod_category.cat_id=qod_subscriptions.sub_category_id"
            );
            $join = array(
                $join1
            );
            $got = $this->common_model->join('*', "qod_subscriptions", $where, "qod_subscriptions.sub_added_on DESC", "10", $join);
            if (empty($got)) {
                $got = [];
            }
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
            die;
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function quotes_list_post() {
        $data = "";
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_category_id', 'Category id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $where = array("DATE(qq_added_on) ='" . date("Y-m-d") . "'", "qq_category_id='" . $post['sub_category_id'] . "'", "qq_pay_type='1'");
            $got = $this->common_model->select('*', "qod_quotes", $where, "qod_quotes.qq_added_on ASC");
            $where1 = array("sub_end_date >='" . date("Y-m-d H:i:s") . "'", "sub_user_id = '" . $post['user_id'] . "'", "sub_category_id = '" . $post['sub_category_id'] . "'");
            $sub_user_count = $this->common_model->select('COUNT(sub_user_id) as count', "qod_subscriptions", $where1);
            $i = 0;
            $quote = array();
            if (isset($got) && $got != "") {
                foreach ($got as $val) {
                    if ($sub_user_count[0]['count'] > $i) {
                        $quote[] = $val;
                    }
                    $i++;
                }
            }
            $base = base_url() . "uploads/admin/quotes/";
            $data['list'] = $quote;
            $data['base'] = $base;
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            die;
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function general_quote_list_post() {
        $list = $this->common_model->select("*", "qod_quotes", array("qq_pay_type='0'"), "", "10");
        if ($list) {
            $base = base_url() . "uploads/admin/quotes/";
            $data['list'] = $list;
            $data['base'] = $base;
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            die;
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
    }

}
