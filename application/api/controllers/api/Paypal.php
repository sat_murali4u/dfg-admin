<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Paypal extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'security', 'email', 'string', 'language'));
        $this->load->library(array('form_validation', 'curl'));

        $this->load->model('common_model');
        define('STATUS', 'payment_status');
        define('MESSAGE', 'payment_msg');
    }

    function _headercall($header_array) {
        if (isset($header_array) && !empty($header_array)) {
            foreach ($header_array as $key => $header) {
                $this->curl->http_header($key, $header);
            }
        }
    }

    function _auth($auth) {
        if (isset($auth) && !empty($auth)) {
            $this->curl->http_login($auth['uname'], $auth['pwd']);
        }
    }

    function _post($post) {
        if (isset($post) && !empty($post)) {
            $this->curl->post($post);
        }
    }

    public function paymentrec_post() {
        $postdata = $this->input->post();
        $check_paypal_id = $this->common_model->select("paypal_payid", "payment", array("paypal_payid" => $postdata['payid']));
        if ($check_paypal_id) {
            header(STATUS . ': 0');
            header(MESSAGE . ': Failer');
            $this->response(apiresponce(0, 'Error', 'Some this Wrong'), REST_Controller::HTTP_OK);
            die;
        }
        //test
        $header = array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Basic QVRBc19ldE84Vi1LU2MwX0J1SjY3X0d0UWZ0eUNFWUV5SUpPVTNSVndpMHJSOElBdzJGQ2FETDVlXzBRQ0JOekN4UHdSYWFBMEtsSXd2UXY6RUFXUXpsQ0NNS3dpbm9fcVViRVAzZEg1RUlXUXlaVHpoT0l5bks2X1FheElDUTRBY25jbDF5eldVNVdQTnVBN1gyTDJkMnRZNFdObGR6QzQ=',
        );
        //live
//        $header = array(
//            'Content-Type' => 'application/x-www-form-urlencoded',
//            'Authorization' => 'Basic QWQybzgydnRoaFBISHRMUmRwSk9FQXh3d0h1MVJ0eUZadm9pNEFLWVpMUE90ckpxdHZwTVMzNWVsMVJmR25SNjVRUlUxYlZRSDM1NE9fVC06RURLUDNLS2lfMl9xRmxsbEdTYUpseFRrSk52WUZoTHRoczFZb09mbi0wRWxWMHE2ZG1WYWkzbTlUbmRoSUtDWUg1UFhFcXd3eVRZbDhFS2I=',
//       );
        $this->_headercall($header);
//        test acc details
        $http_login = array(
            'uname' => 'ATAs_etO8V-KSc0_BuJ67_GtQftyCEYEyIJOU3RVwi0rR8IAw2FCaDL5e_0QCBNzCxPwRaaA0KlIwvQv',
            'pwd' => 'EAWQzlCCMKwino_qUbEP3dH5EIWQyZTzhOIynK6_QaxICQ4Acncl1yzWU5WPNuA7X2L2d2tY4WNldzC4',
        );
//        live acc details
//        $http_login = array(
//            'uname' => 'Afhbq7PfiipLDLHbLX1os9zj9QKrgoesFGN8WoWNiUsm_SXN1_QrZNZYkJuuPnPG1MUzMdCp7iPtD_BX',
//            'pwd' => 'EDOVVwqEFaU-Ncd4_Fk-8ywNBD9H4Bi0qOXlY_D-b_KyA5QHrjJnK4AbsRaH2d383nFxw_pjqBKnyY1Y',
//        );
        $this->_auth($http_login);
        $postingdata = array('grant_type' => 'client_credentials');

        $this->curl->post($postingdata);
        $this->curl->create('https://api.sandbox.paypal.com/v1/oauth2/token');
//        $this->curl->create('https://api.paypal.com/v1/oauth2/token');
        $result = $this->curl->execute();
        $params = json_decode($result, true);
        $header2 = array(
            'Content-Type' => 'application/json',
            'Authorization' => $params['token_type'] . ' ' . $params['access_token'],
        );
        $url2 = 'https://api.sandbox.paypal.com/v1/payments/payment/' . $postdata['payid'];
//        $url2 = 'https://api.paypal.com/v1/payments/payment/' . $postdata['payid'];
        $this->_headercall($header2);
        $this->curl->create($url2);
        $result2 = $this->curl->execute();
        $params2 = json_decode($result2, true);
//        print_r($params2);die;
        if (!empty($params2) && $params2['state'] != '') {
            $get_posted_parematstatrt = $this->common_model->select("pay_initiate_posted,pay_type", "payment", array("pay_id" => $postdata['order']));
            $posted = $get_posted_parematstatrt[0]['pay_initiate_posted'];
            $posted = (array) json_decode($posted);
            extract($posted);
            $array = array(
                'pay_state' => $params2['state'],
                'paypal_payid' => $postdata['payid'],
                'pay_json' => $result2,
                'pay_updated_on' => date('Y-m-d H:i:s'),
            );
            $update = array();
            $update['sub_payid'] = $this->common_model->update(array("pay_id" => $postdata['order']), $array, 'payment');
            if ($params2['state'] == 'approved') {
                //success
                if ($get_posted_parematstatrt[0]['pay_type'] == 0) {
                    $res = array();
                    $where1 = array("sub_category_id" => $sub_category_id, "sub_user_id" => $user_id);
                    $count = $this->common_model->select('*', "qod_subscriptions", $where1);
                    $user_data = $this->common_model->select('*', "users", array("id" => $user_id));
                    $user_tz = $user_data[0]['timezone'];
                    $time = '00:00:00';
                    $dire = 0;
                    if ($user_tz != '') {
                        date_default_timezone_set($user_data[0]['timezone']);
                        $current_time = date("H:i:s");
                        $gmtTimezone = new DateTimeZone($user_tz);
                        $myDateTime = new DateTime($sub_quote_time, $gmtTimezone);
                        $sub_quote_utctime = $this->convert_tz($sub_quote_time, $myDateTime->format('P'), '+00:00');
                        $current_utctime = $this->convert_tz($current_time, $myDateTime->format('P'), '+00:00');
                        $datetime = strtotime($sub_quote_utctime);
                        $current_datetime = strtotime($current_utctime);
                        $date = date("Y-m-d", $datetime);
                        $current_date = date("Y-m-d", $current_datetime);
                        $time = date("H:i:s", $datetime);
                        $datediff = strtotime($date) - strtotime($current_date);
                        $df = floor($datediff / (60 * 60 * 24));
                        if ($df == 0) {
                            $dire = 0;
                        } else if ($df > 0) {
                            $dire = 1;
                        } else {
                            $dire = 2;
                        }
                    }
                    $insert_data = array(
                        "sub_user_id" => $user_id,
                        "sub_nick_name" => $sub_nick_name,
                        "sub_category_id" => $sub_category_id,
                        "sub_user_count" => $count != "" ? count($count) + 1 : 1,
                        "sub_start_date" => date('Y-m-d H:i:s'),
                        "sub_quote_time" => $sub_quote_time,
                        "sub_quote_utctime" => $time,
                        "sub_utc_direction" => $dire,
                        "sub_paystatus" => 1,
                        "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
                        "sub_added_on" => date('Y-m-d H:i:s'),
                    );

                    $sub_id = $sub_id = $this->common_model->insert("qod_subscriptions", $insert_data);
                    if ($sub_id) {
                        $this->common_model->update(array("pay_id" => $postdata['order']), array('pay_orderid' => $sub_id), 'payment');
                    }
                    $cat_details = $this->common_model->select("*", "qod_category", array("cat_id" => $sub_category_id));
                    $a = "Boss You Have Subscription in Category " . $cat_details[0]['cat_name'] . "!!!!!!!!";
                    $me = new firebasetest;
                    $fire = $me->fireSet("/heading/", $a);
                    $fire = $me->fireSet("/unique_key/", time());
                    $fires = json_decode($fire);
                } elseif ($get_posted_parematstatrt[0]['pay_type'] == 1) {
                    $update_data = array(
                        "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
                        "sub_updated_on" => date('Y-m-d H:i:s'),
                    );
                    $done = $this->common_model->update(array("sub_id" => $sub_id), $update_data, "qod_subscriptions");
                    if ($done) {
                        $this->common_model->update(array("pay_id" => $postdata['order']), array('pay_orderid' => $sub_id), 'payment');
                    }
                }
//                if (isset($_POST['is_second']) && !empty($is_second)) {
//                    $where1 = array("sub_category_id" => $post['sub_category_id'], "sub_user_id" => $user_id);
//                    $count = $this->common_model->select('*', "qod_subscriptions", $where1);
//                    $insert_data = array(
//                        "sub_user_id" => $post['user_id'],
//                        "sub_nick_name" => $post['sub_nick_name1'],
//                        "sub_category_id" => $post['sub_category_id1'],
//                        "sub_user_count" => $count != "" ? count($count) + 1 : 1,
//                        "sub_start_date" => date('Y-m-d H:i:s'),
//                        "sub_quote_time" => $post['sub_quote_time1'],
//                        "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
//                        "sub_added_on" => date('Y-m-d H:i:s'),
//                    );
//                    $sub_id = $this->common_model->insert("qod_subscriptions", $insert_data);
//                    $cat_details = $this->common_model->select("*", "qod_category", array("cat_id" => $post['sub_category_id1']));
//                    $a = "Boss You Have Subscription in Category " . $cat_details[0]['cat_name'] . "!!!!!!!!";
//                    $me = new firebasetest;
//                    $fire = $me->fireSet("/heading/", $a);
//                    $fire = $me->fireSet("/unique_key/", time());
//                    $fires = json_decode($fire);
////                print_r($fires);
////                die;
//                }
            }
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $sub_id), REST_Controller::HTTP_OK);
        } else {
            // not valid response
            header(STATUS . ': 0');
            header(MESSAGE . ': Failer');
            $this->response(apiresponce(0, 'Error', 'Some this Wrong'), REST_Controller::HTTP_OK);
        }
    }

    function convert_tz($datetime, $actual_timezone, $timezone2view) {
        $userTimezone = new DateTimeZone($actual_timezone);
        $gmtTimezone = new DateTimeZone($timezone2view);
        $myDateTime = new DateTime($datetime, $gmtTimezone);
        $offset = $userTimezone->getOffset($myDateTime);
        $myInterval = DateInterval::createFromDateString((string) $offset . 'seconds');
        if ($myInterval->s > 0) {
            $myDateTime->modify("-" . abs($myInterval->s) . " seconds");
        } else {
            $myDateTime->modify("+" . abs($myInterval->s) . " seconds");
        }
//        $result = $myDateTime->format('H:i:s');
        $result = $myDateTime->format('Y-m-d H:i:s');
//        print_r($result);
//        die;
        return $result;
    }

}
