<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Qod_vt extends REST_Controller {

    public $subscriptionamt = '';

    function __construct() {
        parent::__construct();
//        $this->load->model(array(""));
        $this->load->helper(array('form', 'url', 'firebase_notify'));
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->load->helper('email');
        $this->load->helper('string');
        $this->load->model('common_model');

        //amt 
        $this->subscriptionamt = 3;

        define('STATUS', 'addstatus');
        define('MESSAGE', 'addmsg');
        $this->load->helper(array('url', 'language'));
    }

    function registor_post() {
        $use_username = $this->config->item('use_username', 'tank_auth');
        if ($use_username) {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[' . $this->config->item('username_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|xss_clean|matches[password]');
        $data['errors'] = "";
        $email_activation = $this->config->item('email_activation', 'tank_auth');
        if ($this->form_validation->run()) {        // validation ok
            if (!is_null($data = $this->tank_auth->create_user(
                            $use_username ? $this->form_validation->set_value('username') : '', $this->form_validation->set_value('email'), $this->form_validation->set_value('password'), $email_activation, $this->input->post('timezone')))) {         // success
                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

                if ($email_activation) {         // send "activate" email
                    $data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

//                    $this->_send_email('activate', $data['email'], $data);

                    unset($data['password']); // Clear password (just for any case)
                } else {
                    if ($this->config->item('email_account_details', 'tank_auth')) { // send "welcome" email
//                        $this->_send_email('welcome', $data['email'], $data);
                    }
                    unset($data['password']); // Clear password (just for any case)
                }
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'] = $this->lang->line($v);
            }
        }
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function login_post() {
        $data['login_by_username'] = ($this->config->item('login_by_username', 'tank_auth') AND
                $this->config->item('use_username', 'tank_auth'));
        $data['login_by_email'] = $this->config->item('login_by_email', 'tank_auth');
        $this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
// Get login for counting attempts to login
        if ($this->config->item('login_count_attempts', 'tank_auth') AND ( $login = $this->input->post('login'))) {
            $login = $this->security->xss_clean($login);
        } else {
            $login = '';
        }
        if ($this->form_validation->run()) {        // validation ok
            if ($this->tank_auth->login(
                            $this->form_validation->set_value('login'), $this->form_validation->set_value('password'), $this->form_validation->set_value('remember'), $data['login_by_username'], $data['login_by_email'])) {        // success
                $user_data = $this->common_model->select("*", "users", array("LOWER(email)" => strtolower($this->form_validation->set_value('login'))));
//                echo $this->db->last_query();die;
                unset($user_data[0]['password']);
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $user_data[0]), REST_Controller::HTTP_OK);
                die;
            } else {
                $errors = $this->tank_auth->get_error_message();
                if (isset($errors['banned'])) {        // banned user
                    $data['errors']['banned'] = $this->lang->line('auth_message_banned') . ' ' . $errors['banned'];
                } elseif (isset($errors['not_activated'])) {    // not activated user
                    $data['errors']['activate'] = "Please Activate your account and try again";
                } else {             // fail
                    foreach ($errors as $k => $v)
                        $data['errors'] = $this->lang->line($v);
                }
            }
        }
        unset($data['login_by_username']);
        unset($data['login_by_email']);
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function forgotpassword_post() {
        $this->form_validation->set_rules('login', 'Email or login', 'trim|required|xss_clean');
        $data['errors'] = array();
        if ($this->form_validation->run()) {        // validation ok
            if (!is_null($data = $this->tank_auth->forgot_password(
                            $this->form_validation->set_value('login')))) {

                $data['site_name'] = $this->config->item('website_name', 'tank_auth');

// Send email with password activation link
                $this->_send_email('forgot_password', $data['email'], $data);

                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $this->lang->line('auth_message_new_password_sent')), REST_Controller::HTTP_OK);
                die;
            } else {
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'] = $this->lang->line($v);
            }
        }
        unset($data['login_by_username']);
        unset($data['login_by_email']);
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function changepassword_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length[' . $this->config->item('password_min_length', 'tank_auth') . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');
        $data['errors'] = '';
        if ($this->form_validation->run()) {        // validation ok
            if ($this->tank_auth->change_password(
                            $this->form_validation->set_value('old_password'), $this->form_validation->set_value('new_password'), $this->form_validation->set_value('user_id'))) { // success
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $this->lang->line('auth_message_password_changed')), REST_Controller::HTTP_OK);
                die;
            } else {              // fail
                $errors = $this->tank_auth->get_error_message();
                foreach ($errors as $k => $v)
                    $data['errors'][$k] = $this->lang->line($v);
            }
        }
        $data['validation_err'] = validation_errors();
        $data['validation_err'] = strip_tags($data['validation_err']);
        $data['validation_err'] = str_replace("\n", "", $data['validation_err']);
        $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        $data['errors'] = empty($data['validation_err']) ? "" : $data['validation_err'];
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function _send_email($type, $email, &$data) {
        $this->load->library('email');
        $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
        $this->email->to($email);
        $this->email->subject(sprintf($this->lang->line('auth_subject_' . $type), $this->config->item('website_name', 'tank_auth')));
        $this->email->message($this->load->view('email/' . $type . '-html', $data, TRUE));
        $this->email->set_alt_message($this->load->view('email/' . $type . '-txt', $data, TRUE));
        $this->email->send();
    }

    public function update_id_post() {
        $got = $this->buyerapi_model->select('*', "update_id");
        if ($got) {
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
        } else {
            header(STATUS . ': 0');
            header(MESSAGE . ': failed');
            $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function all_category_post() {
        $got = $this->common_model->select('*', "qod_category");
        if ($got) {
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
        } else {
            header(STATUS . ': 0');
            header(MESSAGE . ': failed');
            $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function create_subscription_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('is_second', 'second_is', 'trim|required|xss_clean');
        if (isset($_POST['is_second']) && !empty($_POST['is_second'])) {
            $this->form_validation->set_rules('sub_nick_name1', 'Nick Name 2', 'trim|required|xss_clean|differs[sub_nick_name]');
            $this->form_validation->set_rules('sub_category_id1', 'Catagory 2', 'trim|required|xss_clean');
            $this->form_validation->set_rules('sub_quote_time1', 'sub_quote_time 2', 'trim|required|xss_clean');
        }
        $this->form_validation->set_rules('sub_nick_name', 'Nick Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_category_id', 'Catagory', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_quote_time', 'sub_quote_time', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $where = array("sub_nick_name" => $post['sub_nick_name'], "sub_user_id" => $post['user_id']);
            $got = $this->common_model->select('*', "qod_subscriptions", $where);
            if ($got) {
                $data['errors']['nick_name'] = "Nick Name Already Exiest";
            } else {
                $res = array();
                $where1 = array("sub_category_id" => $post['sub_category_id'], "sub_user_id" => $post['user_id']);
                $count = $this->common_model->select('*', "qod_subscriptions", $where1);
                $insert_data = array(
                    "sub_user_id" => $post['user_id'],
                    "sub_nick_name" => $post['sub_nick_name'],
                    "sub_category_id" => $post['sub_category_id'],
                    "sub_user_count" => $count != "" ? count($count) + 1 : 1,
                    "sub_start_date" => date('Y-m-d H:i:s'),
                    "sub_quote_time" => $post['sub_quote_time'],
                    "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
                    "sub_added_on" => date('Y-m-d H:i:s'),
                );
                $res['order_id'] = $sub_id = $this->common_model->insert("qod_subscriptions", $insert_data);
                $cat_details = $this->common_model->select("*", "qod_category", array("cat_id" => $post['sub_category_id']));
                $a = "Boss You Have Subscription in Category " . $cat_details[0]['cat_name'] . "!!!!!!!!";
                $me = new firebasetest;
                $fire = $me->fireSet("/heading/", $a);
                $fire = $me->fireSet("/unique_key/", time());
                $fires = json_decode($fire);
                if (isset($_POST['is_second']) && !empty($_POST['is_second'])) {
                    $where1 = array("sub_category_id" => $post['sub_category_id'], "sub_user_id" => $post['user_id']);
                    $count = $this->common_model->select('*', "qod_subscriptions", $where1);
                    $insert_data = array(
                        "sub_user_id" => $post['user_id'],
                        "sub_nick_name" => $post['sub_nick_name1'],
                        "sub_category_id" => $post['sub_category_id1'],
                        "sub_user_count" => $count != "" ? count($count) + 1 : 1,
                        "sub_start_date" => date('Y-m-d H:i:s'),
                        "sub_quote_time" => $post['sub_quote_time1'],
                        "sub_end_date" => date('Y-m-d H:i:s', strtotime("+12 months", strtotime(date('Y-m-d H:i:s')))),
                        "sub_added_on" => date('Y-m-d H:i:s'),
                    );
                    $sub_id = $this->common_model->insert("qod_subscriptions", $insert_data);
                    $cat_details = $this->common_model->select("*", "qod_category", array("cat_id" => $post['sub_category_id1']));
                    $a = "Boss You Have Subscription in Category " . $cat_details[0]['cat_name'] . "!!!!!!!!";
                    $me = new firebasetest;
                    $fire = $me->fireSet("/heading/", $a);
                    $fire = $me->fireSet("/unique_key/", time());
                    $fires = json_decode($fire);
//                print_r($fires);
//                die;
                }

                $res['amt'] = $this->subscriptionamt;
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', $res), REST_Controller::HTTP_OK);
                die;
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function subscription_list_post() {
        $data = "";
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('scroll_id', 'scroll_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            if ($post['scroll_id'] == 0 || $post['scroll_id'] == "") {
                $where = array("sub_user_id " => $post['user_id'], "sub_end_date >=" => date('Y-m-d H:i:s'), "sub_is_deleted" => 0);
            } else {
                $where = array("sub_user_id " => $post['user_id'], "sub_id <" => $post['scroll_id'], "sub_end_date >=" => date('Y-m-d H:i:s'), "sub_is_deleted" => 0);
            }
            $join1 = array(
                "table" => "qod_category",
                "join_string" => "qod_category.cat_id=qod_subscriptions.sub_category_id"
            );
            $join = array(
                $join1
            );
            $got = $this->common_model->join('*', "qod_subscriptions", $where, "qod_subscriptions.sub_added_on DESC", "10", $join);
            if (empty($got)) {
                $got = [];
            }
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $got), REST_Controller::HTTP_OK);
            die;
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    public function quotes_list_post() {
        $data = "";
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_category_id', 'Category id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_id', 'sub_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $fav = $this->favourite_quote_list($post['user_id'], $post['sub_category_id'], $post['sub_id']);
            $where = array("DATE(qq_quote_date)" => date("Y-m-d"), "qq_category_id" => $post['sub_category_id'], "qq_pay_type" => 1);
            $got = $this->common_model->select('*', "qod_quotes", $where, "qod_quotes.qq_added_on ASC");
            $where1 = array("sub_end_date >" => date("Y-m-d H:i:s"), "sub_user_id" => $post['user_id'], "sub_category_id" => $post['sub_category_id'], "sub_id" => $post['sub_id'], "sub_quote_time<=" => date('H:i:s'));
            $sub_details = $this->common_model->select('*', "qod_subscriptions", $where1);
            $quote = array();
            if (isset($got) && $got != "" && $sub_details) {
                $where2 = array("DATE(added_on)" => date("Y-m-d"), "qh_user_id" => $post['user_id'], "qh_category_id" => $post['sub_category_id'], "qh_sub_id" => $post['sub_id']);
                $history_detail = $this->common_model->select('*', "qod_quotes_history", $where2);
                if ($history_detail) {
                    $v['fav'] = 0;
                    foreach ($got as $v) {
                        if ($v['qq_id'] == $history_detail[0]['qh_quote_id']) {
                            if (isset($fav) && !empty($fav)) {
                                foreach ($fav as $va) {
                                    if ($va['qfq_quote_id'] == $v['qq_id']) {
                                        $v['fav'] = 1;
                                    }
                                }
                            } else {
                                $v['fav'] = 0;
                            }
                            $quote[] = $v;
                        }
                    }
                } else {
                    $where3 = array("DATE(added_on)" => date("Y-m-d"), "qh_user_id" => $post['user_id'], "qh_category_id" => $post['sub_category_id']);
                    $history_details = $this->common_model->select('COUNT(qh_id) as bq', "qod_quotes_history", $where3);
                    if (isset($got[$history_details[0]['bq']]) && !empty($got[$history_details[0]['bq']])) {
                        $quote[] = $got[$history_details[0]['bq']];
                        $insert_data = array(
                            'qh_user_id' => $post['user_id'],
                            'qh_category_id' => $post['sub_category_id'],
                            'qh_sub_id' => $post['sub_id'],
                            'qh_quote_id' => $got[$history_details[0]['bq']]['qq_id'],
                            'added_on' => date('Y-m-d H:i:s')
                        );
                        $sub_id = $this->common_model->insert("qod_quotes_history", $insert_data);
                    }
                }
            }
            $base = base_url() . "uploads/admin/quotes/";
            $data['list'] = $quote;
            $data['base'] = $base;
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            die;
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function general_quote_list_post() {
        $list = $this->common_model->select("*", "qod_quotes", array("qq_pay_type" => 0), "", "10");
        if ($list) {
            $base = base_url() . "uploads/admin/quotes/";
            $data['list'] = $list;
            $data['base'] = $base;
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            die;
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', ""), REST_Controller::HTTP_NOT_FOUND);
    }

    function create_favourite_quote_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('quote_id', 'quote_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('category_id', 'category_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('fav_code', 'fav_code', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('sub_id', 'sub_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            extract($this->input->post());
            $insert_data = array(
                "qfq_user_id" => $user_id,
                "qfq_quote_id" => $quote_id,
                "qfq_category_id" => $category_id,
//                "qfq_sub_id" => $sub_id,
                "qfq_fav_on" => date('Y-m-d H:i:s'),
                "qfq_status" => $fav_code
            );
            $where = array("qfq_user_id" => $user_id, "qfq_quote_id" => $quote_id);
            $thr = $this->common_model->select("*", 'qod_fav_quote', $where);
            if ($thr) {
                $insert_data['updated_on'] = date('Y-m-d H:i:s');
                $done = $this->common_model->update(array("qfq_id" => $thr[0]['qfq_id']), $insert_data, "qod_fav_quote");
            } else {
                $insert_data['added_on'] = date('Y-m-d H:i:s');
                $done = $this->common_model->insert("qod_fav_quote", $insert_data);
            }
            if ($done) {
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', ""), REST_Controller::HTTP_OK);
                die;
            } else {
                $data['errors'] = "Action Failed,Try Again Later!";
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function favourite_quote_list_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
//        $this->form_validation->set_rules('category_id', 'category_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('scroll_id', 'scroll_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            extract($this->input->post());
            $where_type1 = "";
            $where = array("qfq_user_id" => $user_id, "qfq_status" => 1);
            if (isset($category_id) && $category_id != "") {
                $where['qfq_category_id'] = $category_id;
            }
            

            if ($scroll_id != 0) {
                $where['qfq_id <'] = $scroll_id;
            }
            $join1 = array(
                "table" => "qod_quotes",
                "join_string" => "qod_quotes.qq_id=qod_fav_quote.qfq_quote_id"
            );
            $join2 = array(
                "table" => "qod_subscriptions",
                "join_string" => "qod_subscriptions.sub_category_id=qod_fav_quote.qfq_category_id"
            );
            if (isset($sub_id) && $sub_id != "") {
                $where['sub_id'] = $sub_id;
                $join = array(
                    $join1, $join2
                );
            } else {
                $join = array(
                    $join1
                );
            }

            $got = $this->common_model->join('*', "qod_fav_quote", $where, "qod_fav_quote.qfq_fav_on DESC", "3", $join);
            if (empty($got)) {
                $got = [];
            }
            $base = base_url() . "uploads/admin/quotes/";
            $data['list'] = $got;
            $data['base'] = $base;
            header(STATUS . ': 1');
            header(MESSAGE . ': Success');
            $this->response(apiresponce(1, 'Success', $data), REST_Controller::HTTP_OK);
            die;
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function favourite_quote_list($user_id, $category_id, $sub_id) {
        $where = array("qfq_user_id" => $user_id, "qfq_status" => 1);
        if (isset($category_id) && $category_id != "") {
            $where['qfq_category_id'] = $category_id;
        }

        $join1 = array(
            "table" => "qod_quotes",
            "join_string" => "qod_quotes.qq_id=qod_fav_quote.qfq_quote_id"
        );
        $join2 = array(
            "table" => "qod_subscriptions",
            "join_string" => "qod_subscriptions.sub_category_id=qod_fav_quote.qfq_category_id"
        );
        if (isset($sub_id) && $sub_id != "") {
            $where['sub_id'] = $sub_id;
            $join = array(
                $join1, $join2
            );
        } else {
            $join = array(
                $join1
            );
        }
        $got = $this->common_model->join('*', "qod_fav_quote", $where, "qod_fav_quote.qfq_fav_on DESC", "", $join);
        if (empty($got)) {
            $got = [];
        }
        return $got;
    }

    function update_subscriptions_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_id', 'sub_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_quote_time', 'sub_quote_time', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_nick_name', 'sub_nick_name', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            extract($this->input->post());
            $post = $this->input->post();
            $where = array("sub_nick_name" => $post['sub_nick_name'], "sub_user_id" => $post['user_id'], "sub_id !=" => $sub_id);
            $got = $this->common_model->select('*', "qod_subscriptions", $where);
            if ($got) {
                $data['errors'] = "Nick Name Already Exiest";
            } else {
                $update_data = array(
                    "sub_nick_name" => $sub_nick_name,
                    "sub_quote_time" => $sub_quote_time,
                    "sub_updated_on" => date('Y-m-m H:i:s')
                );
                $done = $this->common_model->update(array("sub_id" => $sub_id, "sub_user_id" => $user_id), $update_data, "qod_subscriptions");
                if ($done) {
                    header(STATUS . ': 1');
                    header(MESSAGE . ': Success');
                    $this->response(apiresponce(1, 'Success', ""), REST_Controller::HTTP_OK);
                    die;
                } else {
                    $data['errors'] = "Action Failed,Try Again Later!";
                }
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function del_subscriptions_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sub_id', 'sub_id', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            extract($this->input->post());
            $update_data = array(
                "sub_is_deleted" => 1,
                "sub_updated_on" => date('Y-m-m H:i:s')
            );
            $done = $this->common_model->update(array("sub_id" => $sub_id, "sub_user_id" => $user_id), $update_data, "qod_subscriptions");
            if ($done) {
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', ""), REST_Controller::HTTP_OK);
                die;
            } else {
                $data['errors'] = "Action Failed,Try Again Later!";
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function notification_post() {
        $where = array("sub_quote_time <=" => date('H:i:s'), "sub_end_date >=" => date('Y-m-d H:i:s'));
        $join1 = array(
            "table" => "qod_category",
            "join_string" => "qod_category.cat_id=qod_subscriptions.sub_category_id"
        );
        $join2 = array(
            "table" => "qod_gcm_details",
            "join_string" => "qod_gcm_details.qgd_user_id=qod_subscriptions.sub_user_id"
        );
        $join = array(
            $join1, $join2,
        );
        $got = $this->common_model->join('*', "qod_subscriptions", $where, "qod_subscriptions.sub_added_on DESC", "", $join);
        if (!empty($got)) {
            foreach ($got as $val) {
                $res[] = firebasenotify($val['qgd_gcm_id'], "Your Code is ready for category " . $val['cat_name']);
                $this->loglib->logall(array('class' => $this->router->fetch_class(), 'method' => $this->router->fetch_method(), 'data' => 'Notify to ' . $val['sub_user_id'], 'refer' => json_encode($res)));
            }
        }
//        $res = firebasenotify("dlLWk65rsBg:APA91bHla-0hi5TqUuHCzBphpKBBczmXCwoZFHA9gTTCGFbF2qoxWrdGWoVzoXDCdoSsxmGY2joEmODCO4fj-KIukjFj8m6MO6ZzLPPvCiDN78P6SrCji54qo_1VdESGEH1AxliR_fRh");
        print_r($res);
    }

    function savegcm_post() {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gcm_id', 'sub_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('device_id', 'sub_quote_time', 'trim|required|xss_clean');
        $this->form_validation->set_rules('log_id', 'sub_quote_time', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            extract($this->input->post());
            $insert_data = array(
                "qgd_user_id" => $user_id,
                "qgd_gcm_id" => $gcm_id,
                "qgd_device_id" => $device_id,
                "qgd_status" => $log_id == 0 ? 0 : 1,
            );
            $where = array("qgd_user_id='$user_id'", "qgd_device_id" => $device_id, "qgd_status" => 1);
            $thr = $this->common_model->select("*", 'qod_gcm_details', $where);
            if ($thr) {
                $insert_data['qgd_updated_on'] = date('Y-m-d H:i:s');
                $done = $this->common_model->update(array("qgd_id" => $thr[0]['qgd_id']), $insert_data, "qod_gcm_details");
            } else {
                $insert_data['qgd_added_on'] = date('Y-m-d H:i:s');
                $done = $this->common_model->insert("qod_gcm_details", $insert_data);
            }
            if ($done) {
                header(STATUS . ': 1');
                header(MESSAGE . ': Success');
                $this->response(apiresponce(1, 'Success', ""), REST_Controller::HTTP_OK);
                die;
            } else {
                $data['errors'] = "Action Failed,Try Again Later!";
            }
        } else {
            $data['errors'] = validation_errors();
            $data['errors'] = strip_tags($data['errors']);
            $data['errors'] = str_replace("\n", "", $data['errors']);
            $data['errors'] = empty($data['errors']) ? "" : $data['errors'];
        }
        header(STATUS . ': 0');
        header(MESSAGE . ': failed');
        $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
    }

    function test_post() {
        $res[] = firebasenotify($_POST['gcm_key'], "Your Code is ready for category ");
        print_r($res);
        die;
    }

    function updateuserprofile_post() {
        $postdata = $this->input->post();
        $this->form_validation->set_rules('user_id', 'User Id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('timezone', 'Time Zone', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $where = array('id' => $postdata['user_id']);
            $data = array('timezone' => $postdata['timezone']);
            $update = $this->common_model->update($where, $data, 'users');
            if ($update) {
//                $where1 = array('sub_paystatus'=>1,'sub_is_deleted'=>0,'sub_user_id'=>$postdata['user_id']);
//                $subscriptions = $this->common_model->select('*','qod_subscriptions',$where1);
//                if(!empty($subscriptions)){
//                    foreach($subscriptions as $subscrib){
//                        
//                    }
//                }
                header(STATUS . ': 1');
                header(MESSAGE . ': success');
                $this->response(apiresponce(1, 'Success', ''), REST_Controller::HTTP_OK);
            }else{
                header(STATUS . ': 1');
                header(MESSAGE . ': success');
                $this->response(apiresponce(0, 'Update failed', ''), REST_Controller::HTTP_OK);
            }
        } else {
            header(STATUS . ': 0');
            header(MESSAGE . ': failed');
            $this->response(apiresponce(0, 'failed ', $data), REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
