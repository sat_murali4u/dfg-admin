<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->model('common_model');
        $this->load->library('loglib');
    }

    function index() {
        $data['slider_quote'] = $this->common_model->select("*", "testimonial");
        $this->load->view('index', $data);
    }
    function log_all_data(){
        $this->loglib->logall(array('class' => $this->router->fetch_class(), 'method' => $this->router->fetch_method(), 'data' => array('post'=>$this->input->post(),'get'=>$this->input->get())));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */