<?php
$new_password = array(
    'name' => 'new_password',
    'id' => 'new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'class' => "input",
    'data-type' => "password"
);
$confirm_new_password = array(
    'name' => 'confirm_new_password',
    'id' => 'confirm_new_password',
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size' => 30,
    'class' => "input",
    'data-type' => "password"
);
?>

<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>QOD |Password Reset Form</title>
        <base href="<?php echo base_url(); ?>">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'>
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="login-wrap">
            <div class="login-html">
                <?php echo form_open($this->uri->uri_string()); ?>
                <h1 class="heading">QOD</h1>
                <h3 class="heading">Reset Your Password</h3>
                <div class="login-form">
                    <div class="">
                        <div class="group">
                            <label for="user" class="label">New Password</label>
                            <!--<input id="user" type="text" class="input">-->
                            <?php echo form_password($new_password); ?>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Confirm New Password</label>
                            <!--<input id="pass" type="password" class="input" data-type="password">-->
                            <?php echo form_password($confirm_new_password); ?>
                        </div>
                        <div class="group">
                            <input type="submit" class="button" value="Change Password">
                        </div>
                        <div class="hr"></div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </body>
</html>
