<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_Model extends CI_Model {

    function __construct() {

// Call the Model constructor
        parent::__construct();
        $this->load->database();
       
//        $this->userid = $this->session->userdata('user_id');
//        $this->userid = 1;
    }

    function insert($table, $data) {
        $this->db->insert($table, $data);
        if ($this->db->insert_id()) {
            return $insert_id = $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function delete($where, $table) {
        if (isset($where) && is_array($where) && $where != "") {
            $this->db->where($where);
        }
        $this->db->delete($table);
        $afftectedRows = $this->db->affected_rows();
        if ($afftectedRows) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function select($select, $table, $where = FALSE, $order_by = FALSE, $limit = FALSE) {
//        $where = array("student_id = '" . $data['student_id'] . "'", "stu_class_id = '" . $data['stu_class_id'] . "'", "attendance_date = '" . date('Y-m-d') . "'");
//        $check = $this->select("*", "student_attendance", $where);
        $this->db->select($select);
        $this->db->from($table);
        if (isset($where) && is_array($where) && $where != "") {
            $this->db->where($where);
        }
        if (isset($order_by) && $order_by != "") {
            $this->db->order_by($order_by);
        }
        if (isset($limit) && $limit != "") {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $options = $query->result_array();
            return $options;
        } else {
            return false;
        }
    }

    function update($where, $data, $table) {
        if (isset($where) && is_array($where) && $where != "") {
            $this->db->where($where);
        }
        $this->db->update($table, $data);
        $afftectedRows = $this->db->affected_rows();
        if ($afftectedRows > 0) {
            return 1;
        } else {
            return false;
        }
    }

    function join($select, $table, $where = FALSE, $order_by = FALSE, $limit = FALSE, $join, $where_or = FALSE) {
        //        $join1 = array(
//                "table" => "qod_subscriptions",
//                "join_string" => "qod_subscriptions.sub_category_id=qod_quotes.qq_category_id"
//            );
        $this->db->select($select);
        $this->db->from($table);
        if (isset($join) && $join != "") {
            foreach ($join as $value) {
                $this->db->join($value['table'], $value['join_string']);
            }
        }
        if (isset($where) && is_array($where)) {
            $this->db->where($where);
        }
        if (isset($where_or) && is_array($where_or)) {
            $this->db->or_where($where_or);
        }
        if (isset($order_by) && $order_by != "") {
            $this->db->order_by($order_by);
        }
        if (isset($limit) && $limit != "") {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
//         echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            $options = $query->result_array();
            return $options;
        } else {
            return false;
        }
    }

}
