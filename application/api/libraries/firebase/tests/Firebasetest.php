<?php

require_once APPPATH . "libraries/firebase/src/firebaselib.php";

class Firebasetest {

    protected $_firebase;

    // --- set up your own database here
    const DEFAULT_URL = 'https://myfirebaseproject-ecc79.firebaseio.com/';
    const DEFAULT_TOKEN = 'T9lx4MdaAoLbS7DpGxjc6VUFTc1Yhx6Gaq0m1uqM';
    const DEFAULT_TODO_PATH = '/';
    const DELETE_PATH = '/sample';
    const DEFAULT_SET_RESPONSE = '{"name":"Pick the milk","priority":1}';
    const DEFAULT_UPDATE_RESPONSE = '{"name":"Pick the beer","priority":2}';
    const DEFAULT_PUSH_RESPONSE = '{"name":"Pick the LEGO","priority":3}';
    const DEFAULT_DELETE_RESPONSE = 'null';
    const DEFAULT_URI_ERROR = 'You must provide a baseURI variable.';

    public function __construct() {
        $this->_firebase = new Firebaselib(self::DEFAULT_URL, self::DEFAULT_TOKEN);
    }

    public function fireSet($path, $val) {
        $response = $this->_firebase->set($path, $val);
        return $response;
    }

    public function fireGetAfterSet() {
        $response = $this->_firebase->get(self::DEFAULT_TODO_PATH);
        return $response;
    }

    public function fireUpdate() {
        $response = $this->_firebase->update(self::DEFAULT_TODO_PATH, $this->_todoBeer);
        return $response;
    }

    public function fireGetAfterUpdate() {
        $response = $this->_firebase->get(self::DEFAULT_TODO_PATH);
        return $response;
    }

    public function firePush() {
        $response = $this->_firebase->push(self::DEFAULT_TODO_PATH, $this->_todoLEGO);
//        $this->assertRegExp('/{"name"\s?:\s?".*?}/', $response);
        return $response;
    }

    /**
     * @depends testPush
     */
    public function fireGetAfterPush($responseName) {
        $response = $this->_firebase->get(self::DEFAULT_TODO_PATH . '/' . $responseName);
        return $response;
    }

    public function fireDelete() {
        $response = $this->_firebase->delete(self::DELETE_PATH);
        return $response;
    }

    public function fireGetAfterDELETE() {
        $response = $this->_firebase->get(self::DEFAULT_TODO_PATH);
        return $response;
    }

    /**
     * @param $response
     * @return mixed
     */
    private function _parsePushResponse($response) {
        $responseObj = json_decode($response);
        return $responseObj->name;
    }

}
